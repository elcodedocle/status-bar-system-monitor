/*
 * Author: Gael Abadin
 * Description: Displays CPU(use %, avg. clock speed, temp), RAM(Used, Free), NET(Down/s, Up/s) info on the status bar.
 * Version: 21
 * GNOME Shell Tested: 43
 * GNOME Shell Supported: 43, 44
 * Git: https://gitlab.gnome.org/elcodedocle/status-bar-system-monitor
 *
 * Credits: AZZlOl - RezMon - https://github.com/ezyway/RezMon
 * Credits: Michael Knap - System Monitor Tray Indicator - https://github.com/michaelknap/gnome-system-monitor-indicator
 * License: MIT License
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */

// Legacy extension bootstrapping script. Replaces extension.js on legacy builds.

'use strict';

const Gio = imports.gi.Gio;

const {panel} = imports.ui.main;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = imports.misc.extensionUtils.getCurrentExtension();

const {discovered_features, init: init_discovered_features} = Me.imports.modules.features.extension_feature_discovery;
const Collector = Me.imports.modules.core.extension.backend.Collector.Collector;
const Button = Me.imports.modules.core.extension.ui.Button.Button;
const Logger = Me.imports.modules.core.extension.logging.Logger.Logger;

const {
    gettext: _,
} = ExtensionUtils;

const NAME = 'org.gnome.shell.extensions.status-bar-system-monitor';
var _collector;
var _indicator;


function getSettings(name) {
    let GioSSS = Gio.SettingsSchemaSource;
    let schemaSource = GioSSS.new_from_directory(
        Me.dir.get_child("schemas").get_path(),
        GioSSS.get_default(),
        false
    );
    let schemaObj = schemaSource.lookup(
        name, true);
    if (!schemaObj) {
        throw new Error('cannot find schemas');
    }
    return new Gio.Settings({settings_schema: schemaObj});
}

// Enable the extension
function enable() {
    const settings = getSettings(NAME);
    Logger.set_configured_log_level(settings);
    settings.connect('changed::log-level', (settings, _) => {
        Logger.set_configured_log_level(settings);
    });
    ExtensionUtils.initTranslations(Me.metadata.uuid);
    init_discovered_features(settings);
    this._collector = new Collector(settings, discovered_features.collectors);
    this._indicator = new Button(
        settings,
        discovered_features.metrics,
        Me,
        discovered_features.watched_settings
    );
    _indicator.start(Me.metadata);
    panel.addToStatusArea(this.SHORT_NAME, _indicator);
}

// Disable the extension
function disable() {
    _indicator.stop();
    _indicator.destroy();
    _indicator = undefined;
    _collector.stop();
    _collector = undefined;
}
