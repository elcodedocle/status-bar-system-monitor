/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// Legacy extension preferences bootstrapping script. Replaces prefs.js on legacy builds.
const {Gtk} = imports.gi;
const Gio = imports.gi.Gio;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Preferences = Me.imports.modules.core.prefs.Preferences.Preferences;
const {discovered_feature_prefs, init: init_discovered_feature_prefs} = Me.imports.modules.features.feature_prefs_discovery;


const {gettext: _,} = ExtensionUtils;

const NAME = 'org.gnome.shell.extensions.status-bar-system-monitor';


function init(metadata) {
  this.metadata = metadata;
  console.debug(`Initializing ${metadata.name} Preferences`);
  ExtensionUtils.initTranslations(metadata.uuid);
}


function buildPrefsWidget() {
  return new Gtk.Label({
    label: Me.metadata.name,
  });
}

function fillPreferencesWindow(window) {
  const prefs = new MinSysMonPreferences();
  prefs.fillPreferencesWindow(window);
}

function getSettings(name) {
  let GioSSS = Gio.SettingsSchemaSource;
  let schemaSource = GioSSS.new_from_directory(
      Me.dir.get_child("schemas").get_path(),
      GioSSS.get_default(),
      false
  );
  let schemaObj = schemaSource.lookup(
      name, true);
  if (!schemaObj) {
    throw new Error('cannot find schemas');
  }
  return new Gio.Settings({settings_schema: schemaObj});
}

class MinSysMonPreferences {
  fillPreferencesWindow(window)
  {
    init_discovered_feature_prefs();
    this.minSysMonPrefs = new Preferences(discovered_feature_prefs);
    this.minSysMonPrefs.fillPreferencesWindow(window, Me.metadata, getSettings(NAME));
  }

}
