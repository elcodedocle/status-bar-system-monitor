/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import {ExtensionPreferences} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

import Preferences from './modules/core/prefs/Preferences.js';
import {discovered_feature_prefs, init as init_discovered_feature_prefs} from './modules/features/feature_prefs_discovery.js';


// Exports the main extension preferences class containing the extension preferences bootstrapping code
export default class MinSysMonPreferences extends ExtensionPreferences {
  NAME = 'org.gnome.shell.extensions.status-bar-system-monitor';

  fillPreferencesWindow(window) {
    init_discovered_feature_prefs();
    this.minSysMonPrefs = new Preferences(discovered_feature_prefs);
    this.minSysMonPrefs.fillPreferencesWindow(window, this.metadata, this.getSettings(this.NAME))
  }

}
