/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import Adw from 'gi://Adw';

import {gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';
import Builder from '../../../../modules/core/prefs/ui/Builder.js';

export class Preferences {

    fillPreferencesWindow(window, pages, metadata, settings){
        window._settings = settings;
        this.metadata = metadata;

        pages.forEach( page => {
            if (page.get_name() === 'advanced') {
                this._add_advanced_page(page, settings);
            }
        });

    }

    _add_advanced_page(page, settings) {
        const mem = new Adw.PreferencesGroup({
            title: _('RAM'),
            description: _(`Advanced memory settings`),
        });
        page.add(mem);


        Builder.add_action_row_with_suffix(mem, _('RAM polling interval [ms]'),
            '',
            Builder.spin_button(settings.get_int('mem-poll-interval-ms'), 1000000, 1,
                10), true,
            settings, 'mem-poll-interval-ms');
    }

}
