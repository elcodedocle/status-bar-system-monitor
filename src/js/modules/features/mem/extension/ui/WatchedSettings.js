/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// Whatever settings are referenced here are watched to trigger ui refresh on change
export const watched_settings = [
    'mem-used-fraction-digits',
    'mem-available-fraction-digits',
    'mem-used-percentage-fraction-digits',
    'swap-used-fraction-digits',
    'swap-available-fraction-digits',
    'swap-used-percentage-fraction-digits'
];