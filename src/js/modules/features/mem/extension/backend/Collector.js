/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import GLib from 'gi://GLib';

import SettingsWrapper from '../../../../../modules/core/extension/settings/SettingsWrapper.js';
import Logger from '../../../../../modules/core/extension/logging/Logger.js';
import FileReader from '../../../../../modules/common/io/FileReader.js';


// Collector class for RAM (mem) metrics feature.
export class Collector {

    constructor(settings, metrics) {
        this._metrics = metrics;
        this._logger = new Logger("o.g.g.e.status-bar-system-monitor.mem.Collector",
            Logger.LEVEL_VALUES?.[settings.get_string('collector-log-level')]);
        this._logger.log(Logger.DEBUG, 'Starting...');
        this._settings = new SettingsWrapper(this._logger, settings);

        // Initialize parameters/values
        this._init_values();

        // Initialize metrics & start update timers
        this.update();
        this._logger.log(Logger.DEBUG, 'Started.');

    }

    _init_values() {
        this._link_settings();
    }

    _link_settings() {
        this._settings.schema_settings.connect('changed::collector-log-level', (settings, _) => {
            this._logger.level = Logger.LEVEL_VALUES?.[this._settings.get_string('collector-log-level')];
        });
    }

    update() {
        this._logger.log(Logger.TRACE, 'Updating MEM metrics...');
        try {
            const content_lines = FileReader.readLines('/proc/meminfo');
            let mem_total = null;
            let mem_available = null;
            let mem_used;
            let swap_total = null;
            let swap_free = null;
            let swap_used = null;

            content_lines.forEach((line) => {
                let [key, value] = line.split(':');
                if (value) {
                    value = parseInt(value.trim(), 10);
                }

                switch (key) {
                    case 'MemTotal':
                        mem_total = value;
                        break;
                    case 'MemAvailable':
                        mem_available = value;
                        break;
                    case 'SwapTotal':
                        swap_total = value;
                        break;
                    case 'SwapFree':
                        swap_free = value;
                        break;
                }
            });

            if (mem_total !== null && mem_available !== null) {
                mem_used = mem_total - mem_available;
            } else {
                mem_used = 0;
            }

            if (swap_total !== null && swap_total > 0 && swap_free !== null) {
                swap_used = swap_total - swap_free;
            }

            const current_time = GLib.get_monotonic_time();

            this._metrics.mem[0].collected[0].time = current_time;
            this._metrics.mem[0].collected[0].value = mem_used;
            this._metrics.mem[0].collected[0].max = mem_total;
            this._logger.log(Logger.TRACE, `Got RAM used: ${this._metrics.mem[0].collected[0].value}`);

            this._metrics.mem[0].collected[1].time = current_time;
            this._metrics.mem[0].collected[1].value = mem_available;
            this._metrics.mem[0].collected[1].max = mem_total;

            this._metrics.mem[0].collected[2].time = current_time;
            this._metrics.mem[0].collected[2].value = mem_used / mem_total * 100;
            this._logger.log(Logger.TRACE, `Got RAM percent: ${this._metrics.mem[0].collected[2].value}`);

            this._metrics.mem[1].collected[0].time = current_time;
            this._metrics.mem[1].collected[0].value = swap_used;
            this._metrics.mem[1].collected[0].max = swap_total;
            this._logger.log(Logger.TRACE, `Got SWAP used: ${this._metrics.mem[1].collected[0].value}`);

            this._metrics.mem[1].collected[1].time = current_time;
            this._metrics.mem[1].collected[1].value = swap_free;
            this._metrics.mem[1].collected[1].max = swap_total;

            this._metrics.mem[1].collected[2].time = current_time;
            this._metrics.mem[1].collected[2].value = swap_used / swap_total * 100;
            this._logger.log(Logger.TRACE, `Got SWAP percent: ${this._metrics.mem[1].collected[2].value}`);

        } catch (e) {
            this._logger.log(Logger.DEBUG, `Failed to update memory usage.`, e);
        }
        this._logger.log(Logger.TRACE, 'Done updating MEM metrics.');
    }

    // Stop updates
    stop() {
        this._logger.log(Logger.DEBUG, 'Stopping...');
        this._logger.log(Logger.DEBUG, 'Stopped.');
    }
}