/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// TREAT AS CONST
// (Replace property values, not whole object)
export const metrics = {
    mem: [{
        feature: { name: "RAM", label: "RAM" },
        name: 'system-ram',
        active: true,
        collected: [{name: 'mem-used', time: 0, unit: 'KB', max: 0.0, value: 0.0, icon: 'computer-chip-symbolic'},
            {name: 'mem-available', time: 0, unit: 'KB', max: 0.0, value: 0.0, more_is_better: true, icon: 'computer-chip-symbolic'},
            {name: 'mem-used-percentage', time: 0, unit: '%', max: 100, value: 0, icon: 'computer-chip-symbolic'},
        ]
    }, {
        feature: { name: "RAM", label: "RAM" },
        name: 'system-swap',
        active: true,
        collected: [{name: 'swap-used', time: 0, unit: 'KB', max: 0.0, value: 0.0, icon: 'horizontal-arrows-symbolic'},
            {name: 'swap-available', time: 0, unit: 'KB', max: 0.0, value: 0.0, more_is_better: true, icon: 'horizontal-arrows-symbolic'},
            {name: 'swap-used-percentage', time: 0, unit: '%', max: 100, value: 0, icon: 'horizontal-arrows-symbolic'},
        ]
    }]
};
