/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// TREAT AS CONST
// (Replace property values, not whole object)
export const metrics = {
    cpu: [{
        feature: { name: "CPU", label: "CPU" },
        name: 'cpu-1',
        active: true,
        collected: [
            {name: 'cpu-usage', unit: '%', max: 100, value: 0, acc: {used: 0, total: 0}, icon: 'processor-symbolic'},
            {name: 'cpu-freq', unit: 'MHz', max: 0.0, value: 0.0, icon: 'processor-symbolic'},
            {name: 'cpu-temp', unit: 'C', max: 100, value: 0, icon: 'processor-symbolic'},
            {name: 'cpu-governor', unit: '', max: -1, value: '', icon: 'processor-symbolic'},
            {name: 'cpu-energy-performance-preference', unit: '', max: -1, value: '', icon: 'processor-symbolic'}
        ]
    }]
};
