/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';

import SettingsWrapper from '../../../../../modules/core/extension/settings/SettingsWrapper.js';
import Logger from '../../../../../modules/core/extension/logging/Logger.js';
import FileReader from '../../../../../modules/common/io/FileReader.js';


// Collector class for CPU (cpu) metrics feature.
export class Collector {
    static DEFAULT_SENSORS_CHIP = 'coretemp-isa-0000';
    static DEFAULT_SENSORS_FILTER = '(Package id 0)|(Tctl)';
    static DEFAULT_THERMAL_ZONE_PATH = '/sys/class/thermal/thermal_zone0/temp';

    constructor(settings, metrics) {
        this._metrics = metrics;
        this._logger = new Logger("o.g.g.e.status-bar-system-monitor.cpu.Collector",
            Logger.LEVEL_VALUES?.[settings.get_string('collector-log-level')]);
        this._logger.log(Logger.DEBUG, 'Starting...');
        this._settings = new SettingsWrapper(this._logger, settings);

        // Initialize parameters/values
        this._init_values();

        // Initialize metrics & start update timers
        this.update();
        this._start_sensors_poll();
        this._logger.log(Logger.DEBUG, 'Started.');

    }

    _init_values() {
        // Get turbo frequency
        let turbo_frequency = 0.0;
        try {
            let [result, output,] = GLib.spawn_command_line_sync('bash -c "lscpu | grep MHz"');
            const lscpuOutput = result ?
                new TextDecoder("utf-8").decode(new Uint8Array(output)).trim() : '';
            turbo_frequency = parseFloat(/CPU max MHz:\s+(\d+\.\d+)/.exec(lscpuOutput)[1]);
            this._logger.log(Logger.INFO, `TURBO FREQUENCY [MHz]: ${turbo_frequency}`)
        } catch (e) {
            this._logger.log(Logger.SEVERE, `TURBO FREQUENCY FETCH FAILED: `, e);
        }
        this._metrics.cpu[0].collected[1].max = turbo_frequency;
        this._link_settings();
    }

    _link_settings() {
        this._settings.schema_settings.connect('changed::collector-log-level', (settings, _) => {
            this._logger.level = Logger.LEVEL_VALUES?.[this._settings.get_string('collector-log-level')];
        });
    }

    _start_sensors_poll() {
        try {
            this._logger.log(Logger.TRACE, 'Starting sensors temp poll...');
            // CPU Temp  -----------------------------------------------------------------------
            if (!this._settings.get_boolean('use-sensors') ||
                this._settings.get_strv('enabled-features').indexOf('cpu') < 0) {
                // If CPU stats display is disabled, try again in one second
                this._cpu_temp_timeout = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT_IDLE, 1, () => {
                    this._start_sensors_poll();
                    return GLib.SOURCE_REMOVE;
                });
                this._logger.log(Logger.TRACE, 'Sensors temp poll abandoned.');
                return;
            }

            // CPU TEMP - Execute 'sensors' command to get CPU temperature, use coretemp isa chip for better performance
            const sensors_chip_setting = this._settings.get_boolean('override-sensors-chip') ?
                this._settings.get_string('sensors-chip') : '';
            const sensors_chip = sensors_chip_setting || Collector.DEFAULT_SENSORS_CHIP;
            this._proc = new Gio.Subprocess({
                argv: ['sensors', sensors_chip],
                flags: Gio.SubprocessFlags.STDOUT_PIPE
            });
            this._proc.init(null);

            let stdout = new Gio.DataInputStream({
                base_stream: this._proc.get_stdout_pipe()
            });

            stdout.read_line_async(
                GLib.PRIORITY_DEFAULT_IDLE,
                null,
                this._on_sensors_line_read.bind(this)
            );

            // Check the process completion
            this._proc.wait_check_async(null, this._on_temp_poll_end.bind(this));
            this._logger.log(Logger.TRACE, 'Sensors temp poll started.');
        } catch (e) {
            this._logger.log(Logger.DEBUG, 'Sensors temp poll failed.', e);
            this._cpu_temp_timeout = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT_IDLE, 1, () => {
                this._start_sensors_poll();
                return GLib.SOURCE_REMOVE;
            });
        }
    }

    _on_sensors_line_read(stdout, result) {
        try {
            let line = stdout.read_line_finish_utf8(result)[0];
            if (line !== null) {
                if (!this._update_cpu_sensors_temp(line)) {
                    stdout.read_line_async(GLib.PRIORITY_DEFAULT_IDLE, null, this._on_sensors_line_read.bind(this));
                }
            }
        } catch (e) {
            this._logger.log(Logger.DEBUG, `CPU TEMPERATURE UPDATE LINE PARSE FAILED: `, e);
        }
    }

    _update_cpu_sensors_temp(output) {
        try {
            // Define a regular expression pattern to match the CPU temperature
            // This depends on the machine; it should be an (auto?)config setting!
            const sensors_filter_setting = this._settings.get_boolean('override-sensors-filter') ?
                this._settings.get_string('sensors-filter') : '';
            const sensors_filter = sensors_filter_setting || Collector.DEFAULT_SENSORS_FILTER;
            const pattern = new RegExp(`(${sensors_filter}):\\s+[+]?(?<temp>[\\-]?[\\d.]+)°C`);
            const match = pattern.exec(output);

            if (match?.groups?.temp) {
                this._metrics.cpu[0].collected[2].time = GLib.get_monotonic_time();
                this._metrics.cpu[0].collected[2].value = parseFloat(match?.groups?.temp);
                this._logger.log(Logger.TRACE, `Got CPU temp: ${this._metrics.cpu[0].collected[2].value}`);
                return true;
            }
        } catch (e) {
            this._logger.log(Logger.DEBUG, `CPU TEMPERATURE UPDATE FAILED: `, e);
        }
        return false;
    }

    _on_temp_poll_end(proc, result) {
        try {
            proc.wait_check_finish(result);
            this._logger.log(Logger.TRACE, 'Temp poll finished.');
        } catch (e) {
            this._logger.log(Logger.DEBUG, `CPU TEMPERATURE UPDATE COMMAND FAILED: `, e);
        }
        // Set a one-shot timeout to refresh CPU temp asynchronously every 3 seconds only
        // because the sensors command performance sucks balls on some machines
        this._cpu_temp_timeout = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT_IDLE, 3, () => {
            this._start_sensors_poll();
            return GLib.SOURCE_REMOVE;
        });
    }


    _parse_proc_cpuinfo_line(content_line) {
        const fields = content_line.trim().split(/\s+/);
        let mhz_count = 0;
        let cpu_count = 0;
        // check if element 1 and 2 is 'cpu' and 'MHz' in field = "cpu MHz : 3000.000"
        if (fields[0] === 'cpu' && fields[1] === 'MHz') {
            mhz_count += parseInt(fields[3]);
            cpu_count++;
            this._metrics.cpu[0].collected[1].time = GLib.get_monotonic_time();
            this._metrics.cpu[0].collected[1].value = mhz_count / cpu_count;
            this._logger.log(Logger.TRACE, `Got CPU MHz: ${this._metrics.cpu[0].collected[1].value}`);
        }
    }


    _parse_sys_cpufreq_scaling_governor_line(content_line) {
        const fields = content_line.trim().split(/\s+/);
        if (fields[0]) {
            this._metrics.cpu[0].collected[3].time = GLib.get_monotonic_time();
            this._metrics.cpu[0].collected[3].value = content_line.trim();
            this._update_metrics_cpu_feature_label();
            this._logger.log(Logger.TRACE, `Got CPU scaling governor: ${this._metrics.cpu[0].collected[3].value}`);
        }
    }


    _parse_sys_cpufreq_energy_performance_preference_line(content_line) {
        const fields = content_line.trim().split(/\s+/);
        if (fields[0]) {
            this._metrics.cpu[0].collected[4].time = GLib.get_monotonic_time();
            this._metrics.cpu[0].collected[4].value = content_line.trim();
            this._update_metrics_cpu_feature_label();
            this._logger.log(Logger.TRACE,
                `Got CPU energy performance preference: ${this._metrics.cpu[0].collected[4].value}`);
        }
    }

    _update_metrics_cpu_feature_label() {
        if (this._metrics.cpu[0].collected[3].value || this._metrics.cpu[0].collected[4].value) {
            this._metrics.cpu[0].feature.label =
                `${this._metrics.cpu[0].name} [${this._metrics.cpu[0].collected[3].value}/${
                    this._metrics.cpu[0].collected[4].value}]`;
        }
    }

    _parse_proc_stat_line(fields) {
        let current_cpu_used;
        let current_cpu_total;
        let current_cpu_usage = 0;
        const nums = fields.slice(1).map(Number);
        const idle = nums[3];
        const iowait = nums[4] || 0; // Include iowait, defaulting to 0 if not present

        current_cpu_total = nums.slice(0, 4).reduce((a, b) => a + b, 0) + iowait;
        current_cpu_used = current_cpu_total - idle - iowait;

        // Ensure previous values are set on the first run
        this._metrics.cpu[0].collected[0].acc.used = this._metrics.cpu[0].collected[0].acc.used || current_cpu_used;
        this._metrics.cpu[0].collected[0].acc.total = this._metrics.cpu[0].collected[0].acc.total || current_cpu_total;

        // Calculate CPU usage as the difference from the previous measurement
        const total_diff = current_cpu_total - this._metrics.cpu[0].collected[0].acc.total;
        const used_diff = current_cpu_used - this._metrics.cpu[0].collected[0].acc.used;

        if (total_diff > 0) { // Check to avoid division by zero
            current_cpu_usage = (used_diff / total_diff) * 100;
        }

        // Store current values for the next calculation
        this._metrics.cpu[0].collected[0].time = GLib.get_monotonic_time();
        this._metrics.cpu[0].collected[0].acc.used = current_cpu_used;
        this._metrics.cpu[0].collected[0].acc.total = current_cpu_total;
        this._metrics.cpu[0].collected[0].value = current_cpu_usage;
        this._logger.log(Logger.TRACE, `Got CPU usage: ${this._metrics.cpu[0].collected[0].value}`);
    }

    _update_cpu_temp_from_sysfs() {
        const sysfs_thermal_zone_setting = this._settings.get_boolean(
            'override-sysfs-thermal-zone') ? this._settings.get_string('sysfs-thermal-zone') : '';
        const file_path = sysfs_thermal_zone_setting || Collector.DEFAULT_THERMAL_ZONE_PATH;
        try {
            const content_lines = FileReader.readLines(file_path);
            if (content_lines) {
                this._metrics.cpu[0].collected[2].time = GLib.get_monotonic_time();
                this._metrics.cpu[0].collected[2].value = parseFloat(content_lines[0].trim()) / 1000.0;
            }
        } catch (e) {
            this._logger.log(Logger.DEBUG, `PROCESSING ERROR IN FILE: ${file_path}`, e);
        }
    }

    update() {
        this._logger.log(Logger.TRACE, 'Updating CPU metrics...');
        this._update_cpu_usage();
        this._update_cpu_frequency();
        this._update_cpu_governor();
        this._update_cpu_performance_preference();
        this._update_cpu_sysfs_temp();
        this._logger.log(Logger.TRACE, 'Done updating CPU metrics.');

    }

    _update_cpu_sysfs_temp() {
        // CPU TEMP -----------------------------------------------------------------------
        try {
            // only updating when sensors is not updating already
            if (!this._settings.get_boolean('use-sensors')) {
                this._update_cpu_temp_from_sysfs();
            }
        } catch (e) {
            this._logger.log(Logger.DEBUG, `CPU TEMP UPDATE FROM SYSFS FAILED: `, e);
        }
    }

    _update_cpu_performance_preference() {
        // CPU PERFORMANCE PREFERENCE -----------------------------------------------------
        try {
            let content_lines = this._read_lines(
                this._settings.get_string('sysfs-cpu-performance-preference'));

            for (let content_line of content_lines) {
                this._parse_sys_cpufreq_energy_performance_preference_line(content_line);
            }

        } catch (e) {
            this._logger.log(Logger.DEBUG, `CPU PERFORMANCE PREFERENCE UPDATE FAILED: `, e);
        }
    }

    _update_cpu_governor() {
        // CPU GOVERNOR -------------------------------------------------------------------
        try {
            let content_lines = this._read_lines(this._settings.get_string('sysfs-cpu-scaling-governor'));

            for (let content_line of content_lines) {
                this._parse_sys_cpufreq_scaling_governor_line(content_line);
            }

        } catch (e) {
            this._logger.log(Logger.DEBUG, `CPU GOVERNOR UPDATE FAILED: `, e);
        }
    }

    _update_cpu_frequency() {
        // CPU FREQUENCY -----------------------------------------------------------------------
        try {
            let content_lines = this._read_lines('/proc/cpuinfo');

            for (let content_line of content_lines) {
                this._parse_proc_cpuinfo_line(content_line);
            }

        } catch (e) {
            this._logger.log(Logger.DEBUG, `CPU FREQUENCY UPDATE FAILED: `, e);
        }
    }

    _update_cpu_usage() {
        // CPU Usage -----------------------------------------------------------------------
        let stat_content_lines = this._read_lines('/proc/stat');
        try {
            for (let content_line of stat_content_lines) {
                const fields = content_line.trim().split(/\s+/);

                if (fields[0] === 'cpu') {
                    this._parse_proc_stat_line(fields);
                    break; // Break after processing the first 'cpu' line
                }
            }
        } catch (e) {
            this._logger.log(Logger.DEBUG, `CPU USAGE UPDATE FAILED: `, e);
        }
    }

    _read_lines(file_path) {
        let content_lines = [];
        try {
            content_lines = FileReader.readLines(file_path);
        } catch (e) {
            this._logger.log(Logger.DEBUG, `PROCESSING ERROR IN FILE: ${file_path}`, e);
        }
        return content_lines;
    }

    // Stop updates
    stop() {
        this._logger.log(Logger.DEBUG, 'Stopping...');
        this._logger.log(Logger.DEBUG, 'Stopped.');
    }
}