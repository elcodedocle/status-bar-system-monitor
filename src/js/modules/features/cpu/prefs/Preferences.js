/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import Adw from 'gi://Adw';

import {gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';
import Builder from '../../../../modules/core/prefs/ui/Builder.js';

export class Preferences {

    fillPreferencesWindow(window, pages, metadata, settings){
        window._settings = settings;
        this.metadata = metadata;

        pages.forEach( page => {
            if (page.get_name() === 'advanced') {
                this._add_advanced_page(page, settings);
            }
        });

    }

    _add_advanced_page(page, settings) {

        const cpu = new Adw.PreferencesGroup({
            title: _('CPU'),
            description: _(`Advanced CPU settings`),
        });
        page.add(cpu);


        Builder.add_action_row_with_suffix(cpu, _('CPU polling interval [ms]'),
            '',
            Builder.spin_button(settings.get_int('cpu-poll-interval-ms'), 1000000, 1,
                10), true,
            settings, 'cpu-poll-interval-ms');
        let override_thermal_row_list = Builder.add_expandable_switch_row(cpu, settings,
            'override-sysfs-thermal-zone', _('Override thermal zone'),
            _('Override sysfs thermal zone path with a custom value'));
        Builder.add_entry_row(override_thermal_row_list, settings, 'sysfs-thermal-zone',
            _('Thermal zone path override'));
        let sensors_row_list = Builder.add_expandable_switch_row(cpu, settings, 'use-sensors',
            _('Use sensors'),
            _('Use the sensors command for CPU temp calculation - IMPACTS PERFORMANCE'));
        let override_chip_row_list = Builder.add_expandable_switch_row(sensors_row_list, settings,
            'override-sensors-chip', _('Override sensors chip'),
            _('Override the sensors command chip argument with a custom value'));
        Builder.add_entry_row(override_chip_row_list, settings, 'sensors-chip', _('Sensors chip override'));
        let override_filter_row_list = Builder.add_expandable_switch_row(sensors_row_list, settings,
            'override-sensors-filter',
            _('Override sensors response filter'), _('Override the sensors command output filter with a custom value'));
        Builder.add_entry_row(override_filter_row_list, settings, 'sensors-filter',
            _('Sensors response filter override'));


    }

}
