/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// Whatever settings are referenced here are watched to trigger ui refresh on change
export const watched_settings = [
    'net-up-fraction-digits',
    'net-down-fraction-digits'
];