/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';

import SettingsWrapper from '../../../../../modules/core/extension/settings/SettingsWrapper.js';
import Logger from '../../../../../modules/core/extension/logging/Logger.js';
import FileReader from '../../../../../modules/common/io/FileReader.js';

// Collector class for NET (net) metrics feature.
export class Collector {
    static DEFAULT_INET_INTERFACE_MAX_SPEED_IN_MBPS = 1000;

    constructor(settings, metrics) {
        this._metrics = metrics;
        this._logger = new Logger("o.g.g.e.status-bar-system-monitor.net.Collector",
            Logger.LEVEL_VALUES?.[settings.get_string('collector-log-level')]);
        this._logger.log(Logger.DEBUG, 'Starting...');
        this._settings = new SettingsWrapper(this._logger, settings);
        this._enabled = this._settings.get_strv('enabled-features').indexOf('net') >= 0;

        // Initialize parameters/values
        this._init_values();

        // Initialize metrics & start update timers
        this.update();
        this._start_active_inet_interface_poll();
        this._logger.log(Logger.DEBUG, 'Started.');

    }

    _init_values() {
        this._link_settings();
    }

    _link_settings() {
        this._settings.schema_settings.connect('changed::override-active-inet-interface-max-speed', (settings, _) => {
            this._refresh_inet_interface_max_speed();
        });
        this._settings.schema_settings.connect('changed::active-inet-interface-max-speed', (settings, _) => {
            this._refresh_inet_interface_max_speed();
        });
        this._settings.schema_settings.connect('changed::override-active-inet-interface', (settings, _) => {
            this._settings.one_shot_active_interface_polling_trigger = true;
        });
        this._settings.schema_settings.connect('changed::active-inet-interface', (settings, _) => {
            this._settings.one_shot_active_interface_polling_trigger = true;
        });
        this._settings.schema_settings.connect('changed::collector-log-level', (settings, _) => {
            this._logger.level = Logger.LEVEL_VALUES?.[this._settings.get_string('collector-log-level')];
        });
        this._settings.schema_settings.connect('changed::enabled-features', (settings, _) => {
            this._logger.level = Logger.LEVEL_VALUES?.[this._settings.get_string('collector-log-level')];
            if (!this._enabled && this._settings.get_strv('enabled-features').indexOf('net') >= 0
                && !this._settings.get_boolean('active-inet-interface-polling')) {
                // trigger a one-shot detection of the active inet interface, since polling is disabled
                this._settings.one_shot_active_interface_polling_trigger = true;
            }
            this._enabled = this._settings.get_strv('enabled-features').indexOf('net') >= 0;
        });
    }

    _update_inet_interface_max_speed(speed_in_mbps) {
        this._metrics.net[0].collected[0].max = speed_in_mbps * 1024 * 1024 / 8;
        this._metrics.net[0].collected[1].max = speed_in_mbps * 1024 * 1024 / 8;
    }

    _refresh_inet_interface_max_speed() {
        if (this._settings.get_boolean('override-active-inet-interface-max-speed')) {
            this._update_inet_interface_max_speed(this._settings.get_int('active-inet-interface-max-speed'));
        } else {
            this._update_inet_interface_max_speed(Collector.DEFAULT_INET_INTERFACE_MAX_SPEED_IN_MBPS);
        }
    }

    _update_default_active_inet_routed_interface(line) {
        let match = this._settings.get_boolean('override-active-inet-interface') ?
            /(?<ifname>.*?)\s+?.*?\s+?(?<ip>.*?)\//.exec(line)
            : /.*?\s+?.*?\s+?.*?\s+?.*?\s+?(?<ifname>.*?)\s+?.*?\s+?(?<ip>.*?)\s/.exec(line);
        // Get active interface name and ip
        let active_interface_name = this._settings.get_boolean('override-active-inet-interface') ?
            this._settings.get_string('active-inet-interface')
            : match?.groups?.ifname || 'not_found';
        let active_interface_ip = match?.groups?.ip || 'not_found';
        if (active_interface_ip === 'not_found') {
            this._logger.log(Logger.TRACE, `CANNOT FIND ACTIVE INTERFACE IP ON LINE ${line}`);
        } else {
            this._logger.log(Logger.TRACE, `ACTIVE INTERFACE IP: ${active_interface_ip}`);
        }
        if (active_interface_name === 'not_found') {
            this._logger.log(Logger.TRACE, `CANNOT FIND ACTIVE INTERFACE NAME ON LINE ${line}`);
            return false;
        } else {
            this._logger.log(Logger.TRACE, `ACTIVE INTERFACE NAME: ${active_interface_name}`);
        }
        this._metrics.net[0].is_new_interface = active_interface_name !== this._metrics.net[0].name;
        this._metrics.net[0].name = active_interface_name;
        this._metrics.net[0].ip = active_interface_ip;
        this._metrics.net[0].feature.label = `${this._metrics.net[0].name} [${this._metrics.net[0].ip}]`;
        if (this._metrics.net[0].is_new_interface) {
            this._logger.log(Logger.INFO, `NEW ACTIVE INTERFACE NAME: ${this._metrics.net[0].name}`);
            this._logger.log(Logger.INFO, `NEW ACTIVE INTERFACE IP: ${this._metrics.net[0].ip}`);
        }
        return true;
    }

    _start_active_inet_interface_poll() {
        try {
            this._logger.log(Logger.TRACE, 'Starting active inet interface poll...');
            if (this._settings.get_strv('enabled-features').indexOf('net') < 0 ||
                (!this._settings?.one_shot_active_interface_polling_trigger &&
                    !this._settings.get_boolean('active-inet-interface-polling'))) {
                // If NET stats display is disabled, try again in one second
                this._active_inet_interface_poll_timeout = GLib.timeout_add_seconds(
                    GLib.PRIORITY_DEFAULT_IDLE, 1, () => {
                        this._start_active_inet_interface_poll();
                        return GLib.SOURCE_REMOVE;
                    });
                this._logger.log(Logger.TRACE, 'Active inet interface poll abandoned.');
                return;
            }
            this._settings.one_shot_active_interface_polling_trigger = false;

            let command = this._settings.get_boolean('override-active-inet-interface') ?
                ['ip', '-brief', 'address', 'show', this._settings.get_string('active-inet-interface')] :
                ['ip', 'route', 'get', '1']
            // Executes 'ip route get 1' ('ip address show <iface>') command on a Gio.Subprocess to asynchronously get
            // the (default) inet interface (ip).
            // Running this command is stupid expensive and even running it asynchronously it somehow makes display
            // rendering stutter perceptibly on some machines when tested by playing a high resolution video on youtube.
            // Also: This whole thing could probably be some basic promise like
            // GLib.spawn_command_line_async(command).onComplete((stdout) => callback(stdout)).onError((stderr) => error_callback(stderr))
            // I am pretty sure something like it **that allows you to read the command output/exit code** exists
            // already, but I can't find it documented anywhere. Am I wildly overcomplicating this thing?
            // What is Gio.Subprocess made for then? Only long-running processes that need their output monitored??
            this._active_inet_interface_poll_proc = new Gio.Subprocess({
                argv: command,
                flags: Gio.SubprocessFlags.STDOUT_PIPE
            });
            this._active_inet_interface_poll_proc.init(null);

            let stdout = new Gio.DataInputStream({
                base_stream: this._active_inet_interface_poll_proc.get_stdout_pipe()
            });

            stdout.read_line_async(
                GLib.PRIORITY_DEFAULT_IDLE,
                null,
                this._on_ip_route_line_read.bind(this)
            );

            // Check the process completion
            this._active_inet_interface_poll_proc.wait_check_async(null, this._on_ip_route_poll_end.bind(this));

            this._logger.log(Logger.TRACE, 'Active inet interface poll started.');
        } catch (e) {
            this._logger.log(Logger.DEBUG, 'Error starting active inet interface poll.', e);
            this._active_inet_interface_poll_timeout = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT_IDLE, 1, () => {
                this._start_active_inet_interface_poll();
                return GLib.SOURCE_REMOVE;
            });
        }
    }

    _on_ip_route_line_read(stdout, result) {
        try {
            let line = stdout.read_line_finish_utf8(result)[0];
            if (line !== null) {
                if (!this._update_default_active_inet_routed_interface(line)) {
                    stdout.read_line_async(GLib.PRIORITY_DEFAULT_IDLE, null, this._on_ip_route_line_read.bind(this));
                }
            }
        } catch (e) {
            this._logger.log(Logger.DEBUG, `ACTIVE INET INTERFACE UPDATE LINE PARSE FAILED: `, e);
        }
    }

    _on_ip_route_poll_end(proc, result) {
        try {
            proc.wait_check_finish(result);
            this._logger.log(Logger.TRACE, 'Done polling active inet interface.');
        } catch (e) {
            this._logger.log(Logger.DEBUG, `ACTIVE INET INTERFACE UPDATE COMMAND FAILED: `, e);
        }
        // Set a one-shot timeout to refresh the active inet interface asynchronously every 10 seconds only
        // because the `ip route get 1` command performance sucks mega balls on some machines (maybe reading
        // /proc/net/route from procfs instead would be better?)
        this._active_inet_interface_poll_timeout = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT_IDLE, 10, () => {
            this._start_active_inet_interface_poll();
            return GLib.SOURCE_REMOVE;
        });
    }

    _update_net_channel(metric, bytes, current_time, is_new_interface) {
        // metric IS IMMUTABLE HERE. TREAT AS CONST!!!
        try {
            const time_difference = (current_time - metric.time) / 1000000.0;
            let speed = (bytes - metric.acc) / time_difference;
            // update speed only if the bytes measurement is available
            // and no interface/counter rollover has happened since
            if (speed >= 0 && !is_new_interface && current_time !== 0) {
                metric.value = speed;
                this._logger.log(Logger.TRACE, `Got NET ${metric.name}: ${metric.value}`);
            }
            // Store current values for the next calculation
            metric.time = current_time;
            metric.acc = bytes;

        } catch (e) {
            this._logger.log(
                Logger.DEBUG, `Failed to update network traffic speed for ${metric.name} channel`, e);
        }
    }

    update() {
        this._logger.log(Logger.TRACE, 'Updating NET metrics...');
        try {
            if (!this._metrics.net[0].name) {
                return;
            }
            const content_lines = FileReader.readLines('/proc/net/dev');
            let tx_bytes = 0;
            let rx_bytes = 0;
            for (let i = 2; i < content_lines.length; i++) {
                const line = content_lines[i].trim();
                if (line.startsWith(this._metrics.net[0].name)) {
                    const values = line.split(/\s+/);
                    tx_bytes = parseInt(values[9]);
                    rx_bytes = parseInt(values[1]);
                    break;
                }
            }
            // Calculate network traffic speed in bytes per second
            const current_time = GLib.get_monotonic_time();
            this._update_net_channel(
                this._metrics.net[0].collected[0], tx_bytes, current_time, this._metrics.net[0].is_new_interface);
            this._update_net_channel(
                this._metrics.net[0].collected[1], rx_bytes, current_time, this._metrics.net[0].is_new_interface);
            this._metrics.net[0].is_new_interface = false;
        } catch (e) {
            this._logger.log(Logger.DEBUG, `Failed to update network traffic speed.`, e);
        }
        this._logger.log(Logger.TRACE, 'Done updating NET metrics.');
    }

    // Stop updates
    stop() {
        this._logger.log(Logger.DEBUG, 'Stopping...');
        this._logger.log(Logger.DEBUG, 'Stopped.');
    }
}