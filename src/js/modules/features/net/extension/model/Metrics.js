/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// TREAT AS CONST
// (Replace property values, not whole object)
export const metrics = {
    net: [{
        feature: { name: "NET", label: "NET" },
        name: '',
        ip: '',
        is_new_interface: true,
        active: true,
        collected: [
            {name: 'net-up', time: 0, unit: 'B/s', max: 1024 * 1024 * 1000 / 8, value: 0.0, acc: 0, icon: 'vertical-arrows-symbolic'},
            {name: 'net-down', time: 0, unit: 'B/s', max: 1024 * 1024 * 1000 / 8, value: 0.0, acc: 0, icon: 'vertical-arrows-symbolic'}]
    }]
};
