/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import Adw from 'gi://Adw';

import {gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';
import Builder from '../../../../modules/core/prefs/ui/Builder.js';

export class Preferences {

    fillPreferencesWindow(window, pages, metadata, settings){
        window._settings = settings;
        this.metadata = metadata;

        pages.forEach( page => {
            if (page.get_name() === 'advanced') {
                this._add_advanced_page(page, settings);
            }
        });

    }

    _add_advanced_page(page, settings) {
        const net = new Adw.PreferencesGroup({
            title: _('NET'),
            description: _(`Advanced network settings`),
        });
        page.add(net);


        Builder.add_action_row_with_suffix(net, _('NET polling interval [ms]'),
            '',
            Builder.spin_button(settings.get_int('net-poll-interval-ms'), 1000000, 1,
                10), true,
            settings, 'net-poll-interval-ms');
        Builder.add_switch_row(net, settings, 'active-inet-interface-polling', _('Poll for active inet interface'),
            _('Detect inet interface/LAN changes - IMPACTS PERFORMANCE. Hide &amp; show the NET group instead to trigger a one-shot detection'));
        let override_iface_row_list = Builder.add_expandable_switch_row(net, settings,
            'override-active-inet-interface', _('Override active inet interface'),
            _('Override active inet interface with a custom value'));
        Builder.add_entry_row(override_iface_row_list, settings, 'active-inet-interface',
            _('Active inet interface override'));
        let override_max_speed_row_list = Builder.add_expandable_switch_row(net, settings,
            'override-active-inet-interface-max-speed',
            _('Override inet interface max speed'),
            _('Override the active inet interface max speed with a custom value'));
        Builder.add_action_row_with_suffix(override_max_speed_row_list,
            _('Active inet interface max speed override [Mbps]'),
            '',
            Builder.spin_button(settings.get_int('active-inet-interface-max-speed'), 1000000, 10,
                10), true,
            settings, 'active-inet-interface-max-speed');
    }

}
