/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// Currently hardcoded. Turn it into a real discover?

import {Collector as CpuCollector} from '../../modules/features/cpu/extension/backend/Collector.js';
import {metrics as metrics_cpu} from '../../modules/features/cpu/extension/model/Metrics.js';
import {watched_settings as watched_settings_cpu} from '../../modules/features/cpu/extension/ui/WatchedSettings.js';


import {Collector as MemCollector} from '../../modules/features/mem/extension/backend/Collector.js';
import {metrics as metrics_mem} from '../../modules/features/mem/extension/model/Metrics.js';
import {watched_settings as watched_settings_mem} from '../../modules/features/mem/extension/ui/WatchedSettings.js';


import {Collector as NetCollector} from '../../modules/features/net/extension/backend/Collector.js';
import {metrics as metrics_net} from '../../modules/features/net/extension/model/Metrics.js';
import {watched_settings as watched_settings_net} from '../../modules/features/net/extension/ui/WatchedSettings.js';

import {watched_settings as watched_settings_core} from '../../modules/core/extension/ui/WatchedSettings.js';

export const discovered_features = {
    collectors: {},
    metrics: {},
    watched_settings: { 'core': watched_settings_core }
};

const features = {
    'cpu': {
        collector: CpuCollector,
        metrics: metrics_cpu,
        watched_settings: watched_settings_cpu
    },
    'mem': {
        collector: MemCollector,
        metrics: metrics_mem,
        watched_settings: watched_settings_mem
    },
    'net': {
        collector: NetCollector,
        metrics: metrics_net,
        watched_settings: watched_settings_net
    }
};

// Fakes feature discovery until we figure out a way to actually discover features
export function init(settings) {
    for (let feature in features) {
        discovered_features.collectors[feature] = new features[feature].collector(settings, features[feature].metrics);
        discovered_features.metrics[feature] = features[feature].metrics[feature];
        discovered_features.watched_settings[feature] = features[feature].watched_settings;
    }
}