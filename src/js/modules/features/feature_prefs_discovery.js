/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// Currently hardcoded. Turn it into a real discover?

import {Preferences as CpuPreferences} from '../../modules/features/cpu/prefs/Preferences.js';
import {Preferences as MemPreferences} from '../../modules/features/mem/prefs/Preferences.js';
import {Preferences as NetPreferences} from '../../modules/features/net/prefs/Preferences.js';

export const discovered_feature_prefs = [];

const feature_prefs = [
    CpuPreferences,
    MemPreferences,
    NetPreferences
];

export function init() {
    feature_prefs.forEach(
        x => { discovered_feature_prefs.push(new x()); }
    );
}