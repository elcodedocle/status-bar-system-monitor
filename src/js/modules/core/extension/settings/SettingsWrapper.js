/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import Logger from '../../../../modules/core/extension/logging/Logger.js';

// Used to look at setting retrievals and non-prefs-originated changes
// within context, via
// journalctl /usr/bin/gnome-shell -n 10000 -f -o cat
// which is more convenient as a debug tool than the isolated settings logs shown by
// dconf watch /org/gnome/shell/extensions/minsysmon/
export default class SettingsWrapper {
    constructor(logger, settings) {
        this.schema_settings = settings;
        this.one_shot_active_interface_polling_trigger = true;
        this.level = Logger.LEVEL_VALUES?.[settings.get_string('settings-log-level')];
        this._logger = new Logger(logger.name + '.SettingsWrapper', this.level);
        this._link_settings();
    }

    _link_settings() {
        this.schema_settings.connect('changed::settings-log-level', (settings, _) => {
            this._logger.level = Logger.LEVEL_VALUES?.[this.schema_settings.get_string('settings-log-level')];
        });
    }

    get_string(property) {
        this._logger.log(Logger.TRACE, `Trying to get_string ${property}...`);
        const result = this.schema_settings.get_string(property);
        this._logger.log(Logger.TRACE, `got ${result}`);
        return result;
    }

    get_int(property) {
        this._logger.log(Logger.TRACE, `Trying to get_int ${property}...`);
        const result = this.schema_settings.get_int(property);
        this._logger.log(Logger.TRACE, `got ${result}`);
        return result;
    }

    get_boolean(property) {
        this._logger.log(Logger.TRACE, `Trying to get_boolean ${property}...`);
        const result = this.schema_settings.get_boolean(property);
        this._logger.log(Logger.TRACE, `got ${result}`);
        return result;
    }

    get_strv(property) {
        this._logger.log(Logger.TRACE, `Trying to get_strv ${property}...`);
        const result = this.schema_settings.get_strv(property);
        this._logger.log(Logger.TRACE, `got ${result}`);
        return result;
    }

    set_strv(property, value) {
        this._logger.log(Logger.TRACE, `Trying to set_strv ${property} with value ${value}...`);
        this.schema_settings.set_strv(property, value);
    }
}