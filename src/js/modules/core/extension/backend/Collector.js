/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// Collector core: schedules update from different feature sources (collectors), updating their metrics

import GLib from 'gi://GLib';

import SettingsWrapper from '../../../../modules/core/extension/settings/SettingsWrapper.js';
import Logger from '../../../../modules/core/extension/logging/Logger.js';


export default class Collector {

    constructor(settings, collectors) {
        this._collectors = collectors;
        this._logger = new Logger("o.g.g.e.status-bar-system-monitor.core.Collector",
            Logger.LEVEL_VALUES?.[settings.get_string('collector-log-level')]);
        this._logger.log(Logger.DEBUG, 'Starting...');
        this._settings = new SettingsWrapper(this._logger, settings);

        // Initialize parameters/values
        this._init_values();

        // Initialize metrics & start update timers
        this._update_metrics();
        this._logger.log(Logger.DEBUG, 'Started.');

    }

    _init_values() {
        this._link_settings();
    }

    _link_settings() {
        this._settings.schema_settings.connect('changed::collector-log-level', (settings, _) => {
            this._logger.level = Logger.LEVEL_VALUES?.[this._settings.get_string('collector-log-level')];
        });
        this._settings.schema_settings.connect('changed::enabled-features', (settings, _) => {
            this._update_metrics();
        });
    }

    // Update metrics (Doesn't call the function if not activated)
    _update_metrics() {
        this._logger.log(Logger.TRACE, 'Updating metrics...');
        const enabled_features = this._settings.get_strv('enabled-features');
        for (let feature in this._collectors) {
            if (enabled_features.indexOf(feature) >= 0 && !this._collectors[feature]?.timeout) {
                // Start update timer
                this._collectors[feature].timeout = this._timeout_add(feature);
            }
        }
        this._logger.log(Logger.TRACE, 'Done updating metrics.');
        return true;
    }

    _timeout_add(feature) {
        return GLib.timeout_add(
            GLib.PRIORITY_DEFAULT_IDLE, this._settings.get_int(`${feature}-poll-interval-ms`), () => {
                this._collectors[feature].update();
                this._collectors[feature].timeout = this._timeout_add(feature);
                return GLib.SOURCE_REMOVE;
            });
    }

    // Stop updates
    stop() {
        this._logger.log(Logger.DEBUG, 'Stopping...');
        for (let feature in this._collectors) {
            if (this._collectors[feature]?.timeout) {
                GLib.source_remove(this._collectors[feature].timeout);
            }
            this._collectors[feature].timeout = undefined;
            this._collectors[feature].stop();
        }
        this._logger.log(Logger.DEBUG, 'Stopped.');
    }
}