/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// Whatever settings are referenced here are watched to trigger ui refresh on change
export const watched_settings = [
    'colorize-values',
    'high-usage-range-color',
    'medium-usage-range-color',
    'low-usage-range-color',
    'high-threshold',
    'medium-threshold',
    'override-status-line-template-preset',
    'status-line-template',
    'status-line-template-preset',
    'status-line-template-contains-markup'
];