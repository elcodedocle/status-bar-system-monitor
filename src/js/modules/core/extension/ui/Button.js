/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';

import St from 'gi://St';
import Meta from 'gi://Meta';
import Clutter from 'gi://Clutter';
import Shell from 'gi://Shell';
import GObject from 'gi://GObject';

import {gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
import {Button as ButtonBase} from 'resource:///org/gnome/shell/ui/panelMenu.js';
import {PopupMenuItem, PopupSeparatorMenuItem, Ornament} from 'resource:///org/gnome/shell/ui/popupMenu.js';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import Logger from '../../../../modules/core/extension/logging/Logger.js';
import SettingsWrapper from '../../../../modules/core/extension/settings/SettingsWrapper.js';

// UI core, presented as a button with a contextual menu in the status bar.
// Refreshed periodically and on watched settings changes.
export default class Button extends ButtonBase {

    SHORT_NAME = 'status-bar-system-monitor';

    constructor(settings, metrics, extension, watched_settings) {
        super(settings);
        this._extension = extension;
        this._watched_settings = watched_settings;
        this._metrics = metrics;
        this._features = {}; // maybe this should be an array instead so we could sort it from some weight setting
        Object.getOwnPropertyNames(this._metrics).forEach(x => this._features[x] = {
            name: this._metrics[x][0].feature.name,
            label: this._metrics[x][0].feature.label
        });
    }

    _extension_dir() {
        return this._extension.dir;
    }

    _init(settings) {
        this._logger = new Logger("o.g.g.e.status-bar-system-monitor.core.Button",
            Logger.LEVEL_VALUES?.[settings.get_string('status-button-log-level')]);
        this._logger.log(Logger.DEBUG, '_init called...');
        super._init(0, "Mini System Monitor", false);
        this._settings = new SettingsWrapper(this._logger, settings);
        this._logger.log(Logger.DEBUG, '_init complete');
    }

    start(metadata) {

        this._logger.log(Logger.DEBUG, 'Starting...');
        this._metadata = metadata;
        this.resource = Gio.Resource.load(this._extension_dir().get_path() + `/status-bar-system-monitor.gresource`);
        this.resource._register();
        // Create GUI components
        this.box = new St.BoxLayout();
        this.label = new St.Label({
            text: "---Min Sys Mon---",
            y_align: Clutter.ActorAlign.CENTER
        });
        this.box.add_child(this.label);
        this.box_contents = [this.label];
        this.add_child(this.box);
        this._add_contextual_menu();

        // Initialize metrics
        this._write_to_status_bar();

        // Start GUI update timer
        this._timeout = this._timeout_add();

        // link settings to update the status bar on change
        this._link_settings();


        this._logger.log(Logger.DEBUG, 'Started.');

    }

    _timeout_add() {
        return GLib.timeout_add(
            GLib.PRIORITY_DEFAULT_IDLE, this._settings.get_int('gui-update-interval-ms'), () => {
                this._write_to_status_bar();
                this._timeout = this._timeout_add();
                return GLib.SOURCE_REMOVE;
            });
    }

    _link_settings() {
        for (let feature in this._watched_settings) {
            for (const setting in this._watched_settings[feature]) {
                this._settings.schema_settings.connect(`changed::${setting}`, (settings, _) => {
                    this._write_to_status_bar();
                });
            }
        }
        this._settings.schema_settings.connect('changed::status-button-log-level', (settings, _) => {
            this._logger.level = Logger.LEVEL_VALUES?.[this._settings.get_string('status-button-log-level')];
        });
    }

    _update_labels() {
        let has_changed = false;
        for (let feature in this._features) {
            if (this._metrics[feature][0].feature.label !== this._features[feature].label) {
                this._logger.log(Logger.TRACE, `metric label: ${this._metrics[feature][0].feature.label}`);
                this._logger.log(Logger.TRACE, `current feature label: ${this._features[feature].label}`);
                has_changed = true;
                this._features[feature].label = this._metrics[feature][0].feature.label;
            }
        }
        if (has_changed) {
            this._logger.log(Logger.TRACE, 'Updating contextual menu labels...');
            this._add_contextual_menu();
            this._logger.log(Logger.TRACE, 'Done updating contextual menu labels.');
        }
    }

    _add_contextual_menu() {
        this._logger.log(Logger.TRACE, 'Repainting contextual menu...');
        // Remove previous menu entries
        this.menu.removeAll();
        // Create main menu items
        // (This is the reason why perhaps this._features should instead be an array,
        // sorted on some weight setting)
        for (let feature in this._features) {
            const item = new PopupMenuItem(
                this._features[feature].label, {can_focus: true, hover: true, reactive: true});
            item.connect('activate', () => {
                const enabled_features = this._settings.get_strv('enabled-features');
                const feature_index = enabled_features.indexOf(feature);
                feature_index >= 0 ? enabled_features.splice(feature_index, 1) : enabled_features.push(feature);
                this._settings.set_strv('enabled-features', enabled_features);
                this._write_to_status_bar();
                item.setOrnament(
                    enabled_features.indexOf(feature) >= 0 ? Ornament.CHECK : Ornament.NONE
                );
            });
            item.setOrnament(
                this._settings.get_strv('enabled-features').indexOf(feature) >= 0 ?
                    Ornament.CHECK : Ornament.NONE
            );
            this.menu.addMenuItem(item);
        }

        this._update_system_monitor_app();
        if (this._systemMonitorApp) {
            this.menu.addMenuItem(new PopupSeparatorMenuItem());
            const item = new PopupMenuItem(_('System Monitor'), {can_focus: true, hover: true, reactive: true});
            item.connect('activate', () => {
                this._systemMonitorApp.activate();
            });
            this.menu.addMenuItem(item);
        }
        const open_prefs = new PopupMenuItem(_('Settings'), false);
        open_prefs.connect('activate', this._open_preferences.bind(this));
        this.menu.addMenuItem(open_prefs);

        this._logger.log(Logger.TRACE, 'Done repainting contextual menu.');

    }

    _get_open_prefs_window() {
        const windows = global.display.get_tab_list(Meta.TabList.NORMAL_ALL, null);
        for (let win of windows) {
            if (win.get_title().includes(this._metadata.name) &&
                this._get_window_app(win).get_name() === 'Extensions') {
                return {meta_win: win, is_CHCE: true};
            }
            if (win?.wm_class.includes('org.gnome.Shell.Extensions')) {
                return {meta_win: win, is_CHCE: false};
            }
        }
        return {meta_win: null, is_CHCE: null};
    }

    _get_window_app(meta_window) {
        if (!meta_window) {
            return null;
        }
        let tracker = Shell.WindowTracker.get_default();
        return tracker.get_window_app(meta_window);
    }

    _open_preferences() {
        // if prefs window already exist, move it to the current WS and activate it
        const {meta_win, is_CHCE} = this._get_open_prefs_window();
        if (meta_win) {
            if (!is_CHCE) {
                meta_win.delete(global.get_current_time());
            } else {
                this._move_window_to_workspace(meta_win);
                meta_win.activate(global.get_current_time());
                return;
            }
        }

        try {
            Main.extensionManager.openExtensionPrefs(this._metadata.uuid, '', {});
        } catch (e) {
            this._logger.log(Logger.SEVERE, e);
        }
    }

    _move_window_to_workspace(meta_window, workspace = null, monitor_index = -1) {
        let ws = workspace || global.workspace_manager.get_active_workspace();
        let win = meta_window;
        win.change_workspace(ws);
        let target_monitor_index = monitor_index > -1 ?
            monitor_index : global.display.get_current_monitor();
        let current_monitor_index = win.get_monitor();
        if (current_monitor_index !== target_monitor_index) {
            // move window to target monitor
            win.move_to_monitor(target_monitor_index);
        }
    }

    _update_system_monitor_app() {
        const app_system = Shell.AppSystem.get_default();
        this._systemMonitorApp = app_system.lookup_app('gnome-system-monitor.desktop') ||
            app_system.lookup_app('gnome-system-monitor-kde.desktop') ||
            app_system.lookup_app('org.gnome.SystemMonitor.desktop') ||
            app_system.lookup_app('gnome-system-monitor') ||
            app_system.lookup_app('gnome-system-monitor-kde') ||
            app_system.lookup_app('org.gnome.SystemMonitor');
    }

    _write_to_status_bar() {
        this._logger.log(Logger.TRACE, 'Writing to status bar...');
        this._refresh_status_line();
        this._logger.log(Logger.TRACE, 'Writing to status bar done.');
        this._update_labels();
    }

    _color(metric) {
        return this._settings.get_string(`${this._usage_threshold(metric)}-usage-range-color`);
    }

    _usage_threshold(metric) {
        const badness = metric?.more_is_better ? 1 - metric.value / metric.max : metric.value / metric.max;
        const badness_percent = badness * 100;
        if (badness_percent < this._settings.get_int('medium-threshold')) {
            return 'low';
        } else if (badness_percent < this._settings.get_int('high-threshold')) {
            return 'medium';
        }
        return 'high';
    }

    _format(metric, fraction_digits = -1) {
        const autovalue = this._autovalue(metric);
        // ugh...
        const fraction_digits_hack = metric.unit !== '%' && autovalue < 10 && fraction_digits < 1 ?
            1 : fraction_digits;
        if (!this._settings.get_boolean('colorize-values')) {
            return fraction_digits_hack >= 0 ? autovalue.toFixed(fraction_digits_hack) : autovalue;
        }
        return `<span foreground="${this._color(metric)}">${fraction_digits_hack >= 0 ?
            autovalue.toFixed(fraction_digits_hack) : autovalue}</span>`;
    }

    _filtered_template(template) {
        let filtered_template = template;
        for (let feature in this._features) {
            const feature_name = this._features[feature].name;
            // I know.
            const feature_group_regex = new RegExp(
                `((!_.+?!.*?!.+?_!.*?)+(.*?!_.+?!.*?!.+?_!.*?)?(?<feature_prefix>.*?))?(?<${feature}>!_${feature_name}!.*?!${feature_name}_!)((?<feature_suffix>.*?)!_)?`);
            const result = feature_group_regex.exec(filtered_template);
            if (!result.groups?.[feature]) {
                continue;
            }
            const match = result.groups?.feature_prefix ? result.groups.feature_prefix + result.groups[feature] :
                result.groups[feature] + (result.groups?.feature_suffix || '');
            if (this._settings.get_strv('enabled-features').indexOf(feature) < 0) {
                filtered_template = filtered_template.replace(match, '');
            }
        }
        for (let feature in this._features) {
            filtered_template = filtered_template.replace(
                `!_${this._features[feature].name}!`, '').replace(`!${this._features[feature].name}_!`, '');
        }
        this._logger.log(Logger.TRACE, `filtered_template: ${filtered_template}`);
        return filtered_template;
    }

    _encode_html_entities(input) {
        let buf = [];

        for (let i = input.length - 1; i >= 0; i--) {
            if (/[a-zA-Z0-9_\-{}()&#;.:?,=+%![\]]/.test(input[i])) {
                buf.unshift(input[i]);
                continue;
            }
            buf.unshift(['&#', input[i].charCodeAt(), ';'].join(''));
        }

        return buf.join('');
    }

    _autovalue(metric) {
        const result = this._autovalue_inner(metric);
        this._logger.log(Logger.TRACE, `${metric.name} with value ${metric.value} autoscaled to ${result}`);
        return result;
    }

    _autovalue_inner(metric) {
        // TODO: Super lazy ad-hoc quick hack; Please improve
        if (metric?.unit === 'MHz') {
            return metric.value / 1000;
        } else if (metric?.unit === 'KB' || metric?.unit === 'B/s') {
            return metric.value < 1000 ? metric.value / 1024.0 : (metric.value < 1000 * 1024 ? metric.value / 1024 : (
                metric.value < 1000 * 1024 * 1024 ? metric.value / (1024 * 1024) : metric.value / (1024 * 1024 * 1024)));
        }
        return metric.value;
    }

    _unit(metric) {
        const result = this._unit_inner(metric);
        this._logger.log(Logger.TRACE, `${metric.name} unit with value ${metric.unit} autoscaled to ${result}`);
        return result;
    }

    _unit_inner(metric) {
        // TODO: Super lazy ad-hoc quick hack; Please improve
        if (metric?.unit === 'MHz') {
            return 'GHz';
        } else if (metric?.unit === 'C') {
            return '℃';
        } else if (metric?.unit === 'KB') {
            return metric.value < 1000 ? 'MB' : (
                metric.value < 1000 * 1024 ? 'MB' : (metric.value < 1000 * 1024 * 1024 ? 'GB' : 'TB'));
        } else if (metric?.unit === 'B/s') {
            return metric.value < 1000 ? 'KB/s' : (
                metric.value < 1000 * 1024 ? 'KB/s' : (metric.value < 1000 * 1024 * 1024 ? 'MB/s' : 'GB/s'));
        }
        return metric?.unit || '';
    }

    _flattened_metrics() {
        const metrics = [];
        for (let groupkey in this._metrics) {
            for (let subgroup of this._metrics[groupkey]) {
                for (let metric of subgroup.collected) {
                    metrics.push(metric);
                }
            }
        }
        return metrics;
    }

    _check_and_add_placeholder() {
        if (this._settings.get_strv('enabled-features').length > 0) {
            return false;
        }
        this._add_icon('charge-symbolic');
        return true;
    }

    _refresh_status_line() {
        this._logger.log(Logger.TRACE, `Updating status line...`);
        try {
            this._clear_box_contents();
            if (this._check_and_add_placeholder()) {
                return;
            }
            let is_custom_template = this._settings.get_boolean('override-status-line-template-preset');
            const status_line_template_setting = is_custom_template ?
                this._settings.get_string('status-line-template') : '';
            const status_line_template = status_line_template_setting ||
                this._settings.get_strv('status-line-template-presets')[
                    this._settings.get_int('status-line-template-preset')]
            let filtered_template = this._filtered_template(status_line_template);
            if (is_custom_template && !this._settings.get_boolean('status-line-template-contains-markup')) {
                filtered_template = this._encode_html_entities(filtered_template);
            }
            this._add_items(filtered_template);
        } catch (e) {
            this._logger.log(Logger.DEBUG, `Error updating status line...`, e);
        }
        this._logger.log(Logger.TRACE, `Done updating status line...`);
    }

    _clear_box_contents() {
        for (let box_item of this.box_contents) {
            this._logger.log(Logger.TRACE, `Removing box item: ${box_item}`);
            box_item.destroy();
        }
        this.box_contents = [];
    }

    _add_items(filtered_template) {
        let line_items = filtered_template;
        const flattened_metrics = this._flattened_metrics();
        const flattened_metrics_names = flattened_metrics.map(x => x.name);
        const regex = new RegExp(/((?<prefix>(?!{{).*?.){{)|{({(?<tag>.*?)}})|(?<suffix>.*)/);
        let match = regex.exec(line_items);
        let item = match?.groups?.prefix || match?.groups?.tag || match?.groups?.suffix;
        while (item) {
            const metric_name = item.replace('-unit', '').replace('-icon', '');
            this._add_item(match, flattened_metrics_names, metric_name, flattened_metrics, item);
            line_items = line_items.replace(match?.groups?.tag ? `{{${item}}}` : item, '');
            match = regex.exec(line_items);
            item = match?.groups?.prefix || match?.groups?.tag || match?.groups?.suffix;
        }
    }

    _add_item(match, flattened_metrics_names, metric_name, flattened_metrics, item) {
        if (match?.groups?.tag && flattened_metrics_names.indexOf(metric_name) >= 0) {
            this._process_metric_tag(flattened_metrics, flattened_metrics_names, metric_name, item);
        } else {
            this._add_static(item);
        }
    }

    _process_metric_tag(flattened_metrics, flattened_metrics_names, metric_name, item) {
        const metric = flattened_metrics[flattened_metrics_names.indexOf(metric_name)];
        if (item.endsWith('-icon')) {
            if (this._settings.get_boolean('show-icons')) {
                this._add_icon(metric.icon);
            }
            return;
        }
        if (item.endsWith('-unit')) {
            this._add_unit(item, metric);
            return;
        }
        this._add_metric(item, metric);
    }

    _add_static(item) {
        this._logger.log(Logger.TRACE, `Adding static label: ${item}`);
        const label = new St.Label({
            text: '',
            y_align: Clutter.ActorAlign.CENTER
        });
        this.box.add_child(label);
        label.clutter_text.set_markup('\u200B' + item);
        this.box_contents.push(label);
    }

    _add_metric(item, metric) {
        this._logger.log(Logger.TRACE, `Adding metric label: ${item}`);
        const markup = this._format(
            metric,
            this._settings.get_int(metric.name + '-fraction-digits'));
        const label = new St.Label({
            text: '',
            y_align: Clutter.ActorAlign.CENTER
        });
        this.box.add_child(label);
        label.clutter_text.set_markup('\u200B' + markup);
        this.box_contents.push(label);
    }

    _add_unit(item, metric) {
        this._logger.log(Logger.TRACE, `Adding unit label: ${item} for metric: ${metric.name}`);
        const markup = this._encode_html_entities(this._unit(metric));
        const label = new St.Label({
            text: '',
            y_align: Clutter.ActorAlign.CENTER
        });
        this.box.add_child(label);
        label.clutter_text.set_markup('\u200B' + markup);
        this.box_contents.push(label);
    }

    _add_icon(icon_name) {
        try {
            this._logger.log(Logger.TRACE, `Adding icon: ${icon_name}`);
            const file = Gio.File.new_for_uri(`resource:///org/gnome/shell/extensions/elcodedocle/status-bar-system-monitor/icons/${icon_name}.svg`);
            this._logger.log(Logger.TRACE, `Creating icon image from resource: ${icon_name}`);
            this._logger.log(Logger.TRACE, `Creating icon: ${icon_name}`);
            const icon = new St.Icon({
                style_class: 'icon',
                gicon: new Gio.FileIcon({file})
            });
            this._logger.log(Logger.TRACE, `Adding icon: ${icon_name}`);
            this.box.add_child(icon);
            this.box_contents.push(icon);
        } catch (e) {
            this._logger.log(Logger.DEBUG, `Error adding icon ${icon_name}`, e);
        }
    }

    // Stop updates
    stop() {
        this._logger.log(Logger.DEBUG, 'Stopping...');
        if (this._timeout) {
            GLib.source_remove(this._timeout);
        }
        this._timeout = undefined;
        this._logger.log(Logger.DEBUG, 'Stopped.');
    }
}

// Register the button class
GObject.registerClass({GTypeName: 'MinSysMonButton'}, Button);