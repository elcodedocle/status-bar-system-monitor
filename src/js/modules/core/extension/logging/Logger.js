/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
// Logger class. Wraps log calls for easy, fine, dynamic, and component-specific log format/level control
// (Variants of journalctl /usr/bin/gnome-shell command & gnome flags may already allow for some of that, but not all)
export default class Logger {
    static TRACE = 300;
    static DEBUG = 400;
    static INFO = 800;
    static WARNING = 900;
    static SEVERE = 1000;
    static LEVELS = {
        300: "TRACE",
        400: "DEBUG",
        800: "INFO",
        900: "WARNING",
        1000: "SEVERE",
    }
    static LEVEL_VALUES = {
        TRACE: 300,
        DEBUG: 400,
        INFO: 800,
        WARNING: 900,
        SEVERE: 1000,
    }
    static config_level = 800;

    static set_configured_log_level(settings) {
        const settings_level = settings.get_string('log-level');
        if (Logger.LEVEL_VALUES?.[settings_level]) {
            Logger.config_level = Logger.LEVEL_VALUES[settings_level];
        }
    }

    constructor(name = "o.g.g.e.status-bar-system-monitor", level = null) {
        this.name = name;
        this.level = level;
    }

    log(level, message, e = null) {
        const set_level = this.level == null || this.level < Logger.config_level ? Logger.config_level : this.level;
        if (level >= set_level) {
            const datetime = new Date().toISOString();
            if (level >= Logger.SEVERE) {
                if (e) {
                    logError(e, `[${datetime}] ${Logger.LEVELS[level]} ${this.name}: ${message}`);
                } else {
                    console.error(`[${datetime}] ${Logger.LEVELS[level]} ${this.name}: ${message}`);
                }
            } else {
                console.log(`[${datetime}] ${Logger.LEVELS[level]} ${this.name}: ${message}`);
                if (e) {
                    console.log(`[${datetime}] ${Logger.LEVELS[level]} ${this.name}: ${e}`);
                }
            }
        }
    }
}