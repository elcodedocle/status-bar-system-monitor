/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk';
import Adw from 'gi://Adw';

import {gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

export default class Builder {

    static add_entry_row(group, settings, setting, title) {
        // Create a new preference row
        const row = new Adw.EntryRow({
            title: title,
        });
        group.add(row);

        settings.bind(setting, row, 'text',
            Gio.SettingsBindFlags.DEFAULT);
    }

    static add_switch_row(group, settings, setting, title, subtitle) {
        // Create a new preference row
        const row = new Adw.ActionRow({
            title: title,
            subtitle: subtitle,
        });
        group.add(row);
        const row_switch = new Gtk.Switch({
            action_name: `window-list.${setting}`,
            valign: Gtk.Align.CENTER,
        });
        row.add_suffix(row_switch);
        row.set_activatable_widget(row_switch);

        settings.bind(setting, row_switch, 'active',
            Gio.SettingsBindFlags.DEFAULT);
    }

    static add_combo_row(group, settings, setting, title, subtitle, values, save_index = false) {
        // Create a new preference row
        const row = new Adw.ComboRow({
            title: title,
            subtitle: subtitle,
        });
        const model = new Gtk.StringList();
        values.map(x => model.append(x));
        row.set_model(model);
        if (save_index) {
            settings.bind(setting, row, 'selected',
                Gio.SettingsBindFlags.DEFAULT);
        } else {
            row.set_selected(values.indexOf(settings.get_string(setting)));
            settings.connect(`changed::${setting}`, (settings, setting) => {
                row.set_selected(values.indexOf(settings.get_string(setting)));
            });
            row.connect('notify::selected', () => {
                settings.set_string(setting, values[row.get_selected()]);
            });
        }
        group.add(row);
        return row;
    }

    static add_expandable_switch_row(group, settings, setting, title, subtitle) {
        const row_list = new Adw.ExpanderRow({
            title: title,
            subtitle: subtitle,
            show_enable_switch: true
        });
        group.add(row_list);

        settings.bind(setting, row_list, 'enable-expansion',
            Gio.SettingsBindFlags.DEFAULT);

        row_list.add = row_list.add_row;
        return row_list;

    }

    static add_action_row_with_suffix(group, title, subtitle, suffix, activatable = false, settings = null,
                                setting = null) {
        const row = new Adw.ActionRow({
            title: title,
            subtitle: subtitle,
        });
        group.add(row);
        row.add_suffix(suffix);
        if (activatable) {
            row.set_activatable_widget(suffix);
        }
        if (settings && setting) {
            settings.bind(setting, suffix?._is_spin_button ? suffix.adjustment : suffix, 'value',
                Gio.SettingsBindFlags.DEFAULT);
        }
    }

    static add_link_row(group, title, subtitle, uri) {
        const row = new Adw.ActionRow({
            title: title,
            subtitle: subtitle,
        });
        group.add(row);
        const link_button = new Gtk.LinkButton({
            label: '',
            uri,
            halign: Gtk.Align.END,
            valign: Gtk.Align.CENTER,
            hexpand: true,
        });
        row.add_suffix(link_button);
        row.set_activatable_widget(link_button);
    }

    static add_color_picker(group, settings, setting, title, subtitle) {
        // Create a new preference row
        const row = new Adw.ActionRow({
            title: title,
            subtitle: subtitle,
        });
        group.add(row);
        const color_button = new Gtk.ColorButton({
            hexpand: true,
            halign: Gtk.Align.END,
        });
        color_button.set_use_alpha(false);
        color_button._is_color_btn = true;
        let button_rgba = color_button.get_rgba();
        if (button_rgba.parse(settings.get_string(setting))) {
            color_button.set_rgba(button_rgba);
        } else {
            console.error('Could not parse color: ' + settings.get_string(setting));
        }
        color_button.connect('color_set', () => {
            const rgbMatch = /rgb(a)?\((?<r>\d+),(?<g>\d+),(?<b>\d+)/.exec(
                color_button.get_rgba().to_string());
            const htmlColor = "#" +
                parseInt(rgbMatch?.groups?.r).toString(16).padStart(2, '0') +
                parseInt(rgbMatch?.groups?.g).toString(16).padStart(2, '0') +
                parseInt(rgbMatch?.groups?.b).toString(16).padStart(2, '0');
            settings.set_string(setting, htmlColor);
        });
        row.add_suffix(color_button);
        row.set_activatable_widget(color_button);
    }

    static add_slider(group, settings, setting, title, subtitle) {
        // Create a new preference row
        const row = new Adw.ActionRow({
            title: title,
            subtitle: subtitle,
        });
        group.add(row);
        const sliderAdjustment = new Gtk.Adjustment({
            lower: 0,
            upper: 100,
            step_increment: 1,
            page_increment: 5,
            value: settings.get_int(setting),
        });
        const slider = new Gtk.Scale({
            adjustment: sliderAdjustment,
            digits: 0,
            draw_value: true,
            has_origin: true,
            tooltip_text: _(title),
            halign: Gtk.Align.FILL,
            hexpand: true,
        });
        slider.add_mark(25, Gtk.PositionType.BOTTOM, null);
        slider.add_mark(50, Gtk.PositionType.BOTTOM, null);
        slider.add_mark(75, Gtk.PositionType.BOTTOM, null);
        settings.bind(setting, sliderAdjustment, 'value', Gio.SettingsBindFlags.DEFAULT);
        row.add_suffix(slider);
        row.set_activatable_widget(slider);
    }

    static _adjustment(value, upper = 100, step_increment = 1, page_increment = 1) {
        return new Gtk.Adjustment({
            value: value,
            upper: upper,
            step_increment: step_increment,
            page_increment: page_increment,
        });
    }

    static spin_button(value, upper = 100, step_increment = 1, page_increment = 1) {
        let button = new Gtk.SpinButton({
            halign: Gtk.Align.END,
            valign: Gtk.Align.CENTER,
            hexpand: true,
            vexpand: false,
            xalign: 0.5,
        });
        button.set_adjustment(Builder._adjustment(value, upper, step_increment, page_increment));
        button._is_spin_button = true;
        return button;
    }

    static reset_button(settings) {
        const btn = new Gtk.Button({
            halign: Gtk.Align.END,
            valign: Gtk.Align.CENTER,
            hexpand: true,
        });

        const context = btn.get_style_context();
        context.add_class('destructive-action');

        btn.icon_name = 'view-refresh-symbolic';

        btn.connect('clicked', () => {
            Builder._reset(settings);
        });
        btn._activatable = false;
        return btn;
    }

    static label(text){
        const label = new Gtk.Label({
            label: text,
            halign: Gtk.Align.END,
            valign: Gtk.Align.CENTER,
            hexpand: true,
        });
        label._activatable = false;
        return label;
    }

    static _reset(settings) {
        settings.list_keys().forEach(key => settings.reset(key));
    }

}