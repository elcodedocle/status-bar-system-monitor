/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import Adw from 'gi://Adw';

import {gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';
import Builder from '../../../modules/core/prefs/ui/Builder.js';

export default class Preferences {

    _page_data = [
        {
            name: 'appearance',
            title: _('Appearance'),
            icon_name: 'preferences-desktop-appearance-symbolic',
        },
        {
            name: 'advanced',
            title: _('Advanced'),
            icon_name: 'preferences-system-symbolic',
        },
        {
            name: 'developers',
            title: _('Developers'),
            icon_name: 'applications-science-symbolic',
        },
        {
            name: 'about',
            title: _('About'),
            icon_name: 'preferences-system-details-symbolic',
        }
    ];
    _pages = [];

    constructor(feature_prefs) {
        this._feature_prefs = feature_prefs;
    }

    fillPreferencesWindow(window, metadata, settings){
        window._settings = settings;
        this.metadata = metadata;

        for (let page_data of this._page_data) {
            const page = new Adw.PreferencesPage(page_data);
            window.add(page);
            this[`_add_${page_data.name}_page`](page, settings);
            this._pages.push(page);
        }

        this._feature_prefs.forEach(feature => {
            feature.fillPreferencesWindow(window, this._pages, metadata, settings);
        });
    }

    _add_appearance_page(page, settings) {
        const preset = new Adw.PreferencesGroup({
            title: _('Metrics &amp; Decorators'),
            description: _(`Set the layout and displayed metrics for each metrics group of ${this.metadata.name}`),
        });
        page.add(preset);
        const preset_row = Builder.add_combo_row(preset, settings, 'status-line-template-preset',
            _('Status line template preset'), _('The status line template preset'),
            settings.get_strv('status-line-template-preset-names'), true);
        settings.connect('changed::override-status-line-template-preset', (settings, setting) => {
            const is_overridden = settings.get_boolean(setting);
            preset_row.set_sensitive(!is_overridden);
            preset_row.set_subtitle(is_overridden ?
                _('The status line template preset - DISABLED BY OVERRIDE IN ADVANCED PAGE') :
                _('The status line template preset'));
        });
        const style = new Adw.PreferencesGroup({
            title: _('Style'),
            description: _(`Set display style settings such as usage coloring &amp; thresholds`),
        });
        page.add(style);
        Builder.add_switch_row(style, settings, 'show-icons', _('Show icons'),
            _('Show icons where defined in the template'));
        let row_list = Builder.add_expandable_switch_row(style, settings, 'colorize-values',
            _('Colorize values'),
            _('Colorize metric values based on resource usage'));
        Builder.add_slider(row_list, settings, 'medium-threshold', _('Medium threshold'),
            _('max low usage %'));
        Builder.add_slider(row_list, settings, 'high-threshold', _('High threshold'),
            _('min high usage %'));
        Builder.add_color_picker(row_list, settings, 'low-usage-range-color', _('Low Usage Range Color'),
            _('Represents Low Resource Usage'));
        Builder.add_color_picker(row_list, settings, 'medium-usage-range-color', _('Medium Usage Range Color'),
            _('Represents Medium Resource Usage'));
        Builder.add_color_picker(row_list, settings, 'high-usage-range-color', _('High Usage Range Color'),
            _('Represents High Resource Usage'));
    }

    _add_advanced_page(page, settings) {
        const general = new Adw.PreferencesGroup({
            title: _('General'),
            description: _(`Advanced general settings of ${this.metadata.name}`),
        });
        page.add(general);


        Builder.add_action_row_with_suffix(general, _('GUI update interval [ms]'),
            '',
            Builder.spin_button(settings.get_int('gui-update-interval-ms'), 1000000, 1,
                10), true,
            settings, 'gui-update-interval-ms');


        let template_override_row_list = Builder.add_expandable_switch_row(general, settings,
            'override-status-line-template-preset', _('Override status line template'),
            _('Override the status line template with a custom value'));
        Builder.add_entry_row(template_override_row_list, settings, 'status-line-template',
            _('Status line template override'));
        Builder.add_switch_row(template_override_row_list, settings, 'status-line-template-contains-markup',
            _('Status line template markup'), _('The status line template override contains Pango markup'));
    }

    _add_developers_page(page, settings) {
        const overall_log_level = new Adw.PreferencesGroup({
            title: _('Log level'),
            description: _(`Overall log level of ${this.metadata.name}`),
        });
        page.add(overall_log_level);
        const log_levels = ['TRACE', 'DEBUG', 'INFO', 'WARNING', 'SEVERE'];
        Builder.add_combo_row(overall_log_level, settings, 'log-level', _('Log Level'),
            _('Dynamically switch the minimum log level for this extension'), log_levels);
        const component_log_level = new Adw.PreferencesGroup({
            title: _('Component log level'),
            description: _(`Log level for each component of ${this.metadata.name}`),
        });
        page.add(component_log_level);
        Builder.add_combo_row(component_log_level, settings, 'status-button-log-level',
            _('Status Button Log Level'),
            _('Dynamically switch the minimum log level for the status button'), log_levels);
        Builder.add_combo_row(component_log_level, settings, 'collector-log-level', _('Collector Log Level'),
            _('Dynamically switch the minimum log level for the metrics collector'), log_levels);
        Builder.add_combo_row(component_log_level, settings, 'settings-log-level', _('Settings Log Level'),
            _('Dynamically switch the minimum log level for the settings wrapper (not used on preferences)'),
            log_levels);
    }

    _add_about_page(page, settings) {

        const version_name = this.metadata['version-name'] ?? '';
        let version = this.metadata['version'] ?? '';
        version = version_name && version ? `/${version}` : version;
        const version_str = `${version_name}${version}`;

        const general = new Adw.PreferencesGroup({
            title: _(`${this.metadata.name}`),
            description: '',
        });
        page.add(general);

        Builder.add_action_row_with_suffix(
            general,
            _('Version'),
            '',
            Builder.label(version_str)
        );

        Builder.add_action_row_with_suffix(
            general,
            _('Reset all options'),
            _('Reset all options to their default values'),
            Builder.reset_button(settings),
            true
        );


        const links = new Adw.PreferencesGroup({
            title: _('Links'),
            description: '',
        });
        page.add(links);

        Builder.add_link_row(
            links,
            _('Homepage'),
            _('Source code and more info about this extension'),
            'https://gitlab.gnome.org/elcodedocle/status-bar-system-monitor'
        );

        Builder.add_link_row(
            links,
            _('Changelog'),
            _("See what's changed."),
            'https://gitlab.gnome.org/elcodedocle/status-bar-system-monitor/-/blob/main/CHANGELOG.md'
        );

        Builder.add_link_row(
            links,
            _('GNOME Extensions'),
            _('Rate and comment the extension on GNOME Extensions site'),
            'https://extensions.gnome.org/extension/'
        );

        Builder.add_link_row(
            links,
            _('Report a bug or suggest new feature'),
            '',
            'https://gitlab.gnome.org/elcodedocle/status-bar-system-monitor/-/issues'
        );

        Builder.add_link_row(
            links,
            _('Buy Me a Coffee'),
            _('Enjoying this extension? Consider supporting it by buying me a coffee!'),
            'https://bit.ly/elcodedocle-gnome-sysmon-sponsor'
        );

    }

}