/**
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */
import Gio from 'gi://Gio';

export default class FileReader {

    static readLines(file_path) {
        // Get file name and return lines of the file as array
        const file = Gio.File.new_for_path(file_path);
        const [, content] = file.load_contents(null);
        const text_decoder = new TextDecoder("utf-8");
        const content_str = text_decoder.decode(content);
        return content_str.split('\n');
    }

}