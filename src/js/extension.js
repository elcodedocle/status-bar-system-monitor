/*
 * Author: Gael Abadin
 * Description: Displays CPU(use %, avg. clock speed, temp), RAM(Used, Free), NET(Down/s, Up/s) info on the status bar.
 * Version: 121
 * GNOME Shell Tested: 46, 47
 * GNOME Shell Supported: 45, 46, 47
 * Git: https://gitlab.gnome.org/elcodedocle/status-bar-system-monitor
 *
 * Credits: AZZlOl - RezMon - https://github.com/ezyway/RezMon
 * Credits: Michael Knap - System Monitor Tray Indicator - https://github.com/michaelknap/gnome-system-monitor-indicator
 * License: MIT License
 * SPDX-FileCopyrightText: 2024 Gael Abadin <gael.abadin@gmail.com>
 * SPDX-License-Identifier: MIT
 */

'use strict';

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';
import {panel} from 'resource:///org/gnome/shell/ui/main.js';

import Button from './modules/core/extension/ui/Button.js';
import Collector from './modules/core/extension/backend/Collector.js';
import Logger from './modules/core/extension/logging/Logger.js';
import {discovered_features, init as init_discovered_features} from './modules/features/extension_feature_discovery.js';

// Exports the main extension class containing the extension bootstrapping code
export default class SystemMonitorExtension extends Extension {
    NAME = 'org.gnome.shell.extensions.status-bar-system-monitor';
    _collector;
    _indicator;

    // Enable the extension
    enable() {
        const settings = this.getSettings(this.NAME);
        Logger.set_configured_log_level(settings);
        settings.connect('changed::log-level', (settings, _) => {
            Logger.set_configured_log_level(settings);
        });
        init_discovered_features(settings);
        this._collector = new Collector(settings, discovered_features.collectors);
        this._indicator = new Button(
            settings,
            discovered_features.metrics,
            Extension.lookupByUUID(this.metadata.uuid),
            discovered_features.watched_settings
        );
        this._indicator.start(this.metadata);
        panel.addToStatusArea(this.SHORT_NAME, this._indicator);
    }

    // Disable the extension
    disable() {
        this._indicator.stop();
        this._indicator.destroy();
        this._indicator = undefined;
        this._collector.stop();
        this._collector = undefined;
    }
}
