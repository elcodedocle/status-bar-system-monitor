# Development resources

## Extension architecture

### Extension core

This is a simple GNOME extension that periodically (using javascript timeouts) collects metrics through a backend
([Collector](../src/js/modules/core/extension/backend/Collector.js) class) to display on a GNOME status bar button with
a contextual menu ([Button](../src/js/modules/core/extension/ui/Button.js) class), based on some defined preferences
([Preferences](../src/js/modules/core/prefs/Preferences.js) class).

### Extension features

The extension [core](../src/js/modules/core) is itself extended by plugged-in [features](../src/js/modules/features)
(`cpu`, `net`, `mem`,...).

Each feature has its own `Preferences` class, initialised by the
[feature_prefs_discovery.js](../src/js/modules/features/feature_prefs_discovery.js) script.

On top of that, every extension feature requires 3 elements: `Collector` class, `metrics` const, and `WatchedSettings`
const. These are plugged in and initialised by the
[extension_feature_discovery.js](../src/js/modules/features/extension_feature_discovery.js)
script, with the interface described below.

#### Feature `Collector` class

The `Collector` class constructor receives the settings and the metrics model. It must implement an `update` method
that the core [Collector](../src/js/modules/core/extension/backend/Collector.js) class calls periodically to refresh the
metrics model; A `stop` method to perform any actions before the extension stops; And a constructor method that receives
the settings and the model, to initialise the instance when the extension is enabled.

#### Feature `metrics` const

It follows a self-described schema to store the collected metrics that the UI button can process. 

#### Feature `WatchedSettings` const

Just an array of feature setting (string) references that trigger an ui (button) refresh on change. The button is also
self-refreshed periodically.

### GNOME extension architectural components

The [extension.js](../src/js/extension.js) and [extension_legacy.js](../src/js/extension_legacy.js) scripts contain the
bootstrap code to initialise the extension for current and legacy GNOME builds, respectively.
[prefs.js](../src/js/prefs.js) and [prefs_legacy.js](../src/js/prefs_legacy.js).

The [metadata.json](../src/metadata.json) and [metadata_legacy.json](../src/metadata_legacy.json) files follow the GNOME
extension metadata specification described in the
[official GNOME extension docs](https://gjs.guide/extensions/development/creating.html) and
[legacy docs](https://gjs.guide/extensions/upgrading/legacy-documentation.html). The same goes for the
[schema file](../src/schemas/org.gnome.shell.extensions.status-bar-system-monitor.gschema.xml), the translations on the
[po](../po) folder, and the `.gresource` file packed from the resources on the [resources](../src/resources) folder.

## Debugging

Log command: `journalctl /usr/bin/gnome-shell -n 10000 -f -o cat`

Prefs log command: `journalctl /usr/bin/gjs -n 10000 -f -o cat`

Settings log command: `dconf watch /org/gnome/shell/extensions/minsysmon/`

Wayland nested session command: `dbus-run-session -- gnome-shell --nested --wayland`

The extension preferences contains a `Developers` tab to set the log level for different extension components, which is
handled by each component logger, an instance of the [Logger](../src/js/modules/core/extension/logging/Logger.js) class.

The extension handling of settings is wrapped by the
[SettingsWrapper](../src/js/modules/core/extension/settings/SettingsWrapper.js), which facilitates tracking of settings
updates propagation and access by the extension components via extension
[Logger](../src/js/modules/core/extension/logging/Logger.js) log traces.

## Update translations procedure

 1. `make pot`
 2. Open the language translations `.po` file in poedit (E.g. [po/es.po](../po/es.po))
 3. Select `Translation -> Update from .pot file`
 4. Edit/add new translations and save
 5. `make all` will take care of compiling the relevant `.mo` files and including them in the build package

## Building & packaging

For details on the build/packaging mechanism, see the comments on the [Makefile](../Makefile)

### Caveats on import references (BUILD MECHANISM FOR LEGACY GNOME)

1. All imports must use single quotes `'`' instead of double quotes `"`.

2. All relative (project) imports explicitly must contain the full path relative to the `/src/js` folder.

This is for the legacy build script to be able to remap them to the correct legacy import syntax reference.

E.g. The modern import: 

```javascript
import SettingsWrapper from '../../../../modules/core/extension/settings/SettingsWrapper.js';
```

on [/src/js/modules/core/extension/backend/MinSysMonCollector.js](../src/js/modules/core/extension/backend/Collector.js)
will turn into the functioning legacy import when performing a legacy build:

```javascript
const SettingsWrapper = imports.misc.extensionUtils.getCurrentExtension().imports.modules.core.extension.settings.SettingsWrapper.SettingsWrapper;
```

but the equivalent:

```javascript
import SettingsWrapper from "../../../../modules/core/extension/settings/SettingsWrapper.js";
```

will be ignored by the legacy build import mapper because it does not match `"`;

and the equivalent:

```javascript
import SettingsWrapper from '../settings/SettingsWrapper.js';
```

will not work for legacy builds, because the build script will turn it into an invalid (not found) reference missing
the root `modules.core.extension.` part:

```javascript
const SettingsWrapper = imports.misc.extensionUtils.getCurrentExtension().imports.settings.SettingsWrapper.SettingsWrapper;
```

## Links of interest

- https://gjs.guide/extensions/development/creating.html
- https://gjs.guide/extensions/upgrading/legacy-documentation.html#preferences
- https://gjs-docs.gnome.org/javascript/
- https://gjs-docs.gnome.org/adw1~1/
- https://gjs-docs.gnome.org/gtk40~4.0/
- https://gjs-docs.gnome.org/gtk30~3.0/
- https://gjs.guide/guides/gjs/asynchronous-programming.html#promisify-helper
- https://developer.gnome.org/documentation/guidelines/programming/introspection.html
- https://docs.gtk.org/Pango/pango_markup.html
- https://gitlab.gnome.org/GNOME/gnome-shell/-/tree/gnome-47/js/ui
- https://gitlab.gnome.org/GNOME/gnome-shell/-/tree/gnome-46/js/ui
- https://gitlab.gnome.org/GNOME/gnome-shell/-/tree/gnome-44/js/ui
- https://gitlab.gnome.org/GNOME/gjs/
- https://gitlab.gnome.org/GNOME/libgtop
- https://www.npmjs.com/package/@girs/gtop-2.0
- https://docs.gtk.org/#core-libraries
- https://docs.gtk.org/glib/spawn.html
- https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/widget-gallery.html
- https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1.2
