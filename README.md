## Overview
This is a minimalist system monitor extension for GNOME Shell.

It displays metrics on the GNOME Shell top bar; by default:

 - CPU (Usage% | Average Clock | Temperature)
 - RAM (Usage% | UsedGB | Swap%)
 - NET (Download | Upload)

Each group can be enabled/disabled via menu button; or customized via extension config settings.

![Screenshot](./screenshots/full_view.png)

It is a fork (almost a full rewrite) of [RezMon](https://extensions.gnome.org/extension/6952/rezmon/) and
[System Monitor Tray Indicator](https://github.com/michaelknap/gnome-system-monitor-indicator/commits/main/), with
some ideas from [System Monitor](https://extensions.gnome.org/extension/6807/system-monitor/) and some of my own:

 - No GUI blocking IO calls
 - Extra optimizations and bugfixes
 - Fully customizable metrics display
 - Customizable CPU temp source
 - CPU governor indicator on contextual menu
 - Customizable NET interface
 - Customizable polling intervals
 - Inet interface name and IP indicator on contextual menu
 - GNOME system monitor integration via launcher on contextual menu
 - Fully GNOME-backwards compatible (legacy release for GNOME < 45 available)
 - **No unnecessary dependencies**
(No sensors/awk/ethtool/libgtop = should work out of the box on most (linux) distros/versions/setups;
hard to break on system updates)

### Why a fork?

 - Control over design, features, and implementation choices
 - Customizations
 - Compatibility

This couldn't possibly fit in a PR (or many, hence the rewrite), but I still want to give credit where credit is due.

(Can't say it was worth the effort of learning how to write GNOME extensions from scratch, but I am glad I have now
something tailored to my preferences; and learned a new skill)

## Compatibility

This extension was tested on GNOME SHELL 45, 46, & 47 (Arch Linux)
It may work on GNOME SHELL > 47

The GNOME-legacy release was tested on GNOME SHELL 43 & 44 (Debian bookworm)
It may work on GNOME SHELL < 43


## Manual Installation

```bash
cd /tmp
git clone https://gitlab.gnome.org/elcodedocle/status-bar-system-monitor.git
cd status-bar-system-monitor
make clean
make all # For GNOME version < 45, use `make all-legacy` instead
make install
```

Once done, manually restart the GNOME Shell for the changes to take effect. On **X** you can do this by pressing 
`Alt+F2`, typing `r`, and pressing `Enter`. On **Wayland**, simply log out and back in.

The `make clean && make all && make install` script (`make clean && make all-legacy && make install` on GNOME < 45)
copies the extension files to your local GNOME extensions directory. Once GNOME restarts, you can manage the extension
through the Extensions app.

If you want to clean, build, and then (re)deploy on an X Window system running an ssh server with `xdotool` and
`gnome-extensions` cli tool available, you can do so with `make clean-build-redeploy TARGET_HOST=yourdeploymenthost`; or
`make clean-build-redeploy-legacy TARGET_HOST=yourdeploymenthost`, for the legacy version of the extension.


## Resources

### Related projects

There are too many to mention, to the point that I seriously considered not dropping another one into the ecosystem.
Far from an exhaustive or objective list, these are my personal favourites.

#### Most influential (minimalistic)

 - [System Monitor Tray Indicator](https://extensions.gnome.org/extension/6586/system-monitor-tray-indicator/) (The OG sauce)
 - [RezMon](https://extensions.gnome.org/extension/6952/rezmon/) (Fork of the OG; where this started from)
 - [System Monitor](https://extensions.gnome.org/extension/6807/system-monitor/) (Lovely, but no legacy support)
 
#### Best alternatives (feature rich)

 - [System Monitor Next](https://extensions.gnome.org/extension/3010/system-monitor-next/) (Nice graphs; very customizable)
 - [Astra Monitor](https://extensions.gnome.org/extension/6682/astra-monitor/) (The prettiest, and probably also the best)

### Development

 - [GNOME extensions and related development resources](dev-support/RESOURCES.md) (Also explains some import
idiosincrasies required for legacy builds to work)
