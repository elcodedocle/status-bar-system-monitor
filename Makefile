SHELL := /bin/bash

NAME     := status-bar-system-monitor
DOMAIN   := elcodedocle.gitlab.gnome.org
ZIP_FILE := $(NAME)@$(DOMAIN).shell-extension.zip
ZIP_NAME := dist/$(ZIP_FILE)

# Some of the recipes below depend on some of these files.
JS_FILES       = $(shell find ./src/js -type f -and \( -name "*.js" \))
RESOURCE_FILES = $(shell find ./src/resources -mindepth 2 -type f)
LOCALES_PO     = $(wildcard src/po/*.po)
LOCALES_MO     = $(patsubst src/po/%.po,dist/locale/%/LC_MESSAGES/$(NAME).mo,$(LOCALES_PO))

# These files will be included in the extension zip file.
ZIP_CONTENT = $(JS_FILES) \
              $(LOCALES_MO) \
              dist/$(NAME).gresource \
              dist/schemas/gschemas.compiled \
              dist/metadata.json \
              dist/stylesheet.css \
              dist/LICENSE dist/resources/icons/*.license

# Target host in which to (re)deploy the extension zip file **REQUIRES ssh server, gnome-extensions, xdotool, and X Window**
TARGET_HOST?=localhost

# These 9 recipes can be invoked by the user.
.PHONY: clean-build-redeploy clean-build-redeploy-legacy all all-legacy zip install uninstall pot clean

# These are internal composites invoked by user recipes; not intended to be invoked directly.
.PHONY: js js-legacy
.PHONY: js-copy js-legacy-remove js-legacy-rename js-legacy-rewrite-imports-and-exports js-legacy-rewrite-relative-imports
.PHONY: metadata metadata-legacy deploy-common

clean-build-redeploy: clean all deploy-common

clean-build-redeploy-legacy: clean all-legacy deploy-common

all: js metadata $(ZIP_CONTENT) $(ZIP_NAME)

all-legacy: js-legacy metadata-legacy $(ZIP_CONTENT) $(ZIP_NAME)

# The zip recipes only bundles the extension without installing it.
zip: $(ZIP_NAME)

# The install recipes creates the extension zip and installs it.
install: $(ZIP_NAME)
	gnome-extensions install "$(ZIP_NAME)" --force
	@echo "Extension installed successfully! Now restart the Shell ('Alt'+'F2', then 'r' or log out/log in on Wayland)."

# This uninstalls the previously installed extension.
uninstall:
	gnome-extensions uninstall "$(NAME)@$(DOMAIN)"

# Use gettext to generate a translation template file.
pot: $(JS_FILES)
	@echo "Generating '$(NAME).pot'..."
	@xgettext --from-code=UTF-8 \
	          --add-comments=Translators \
	          --copyright-holder="elcodedocle" \
	          --package-name="$(NAME)" \
	          --output=src/po/$(NAME).pot \
	          $(JS_FILES)

# This removes all temporary files created with the other recipes.
clean:
	rm -rf dist/*

# This bundles the extension and checks whether it is small enough to be uploaded to
# extensions.gnome.org.
$(ZIP_NAME): $(ZIP_CONTENT)
	@echo "Packing zip file..."
	@rm --force $(ZIP_NAME)
	@echo "$(EXTRA_SOURCES)"
	@gnome-extensions pack \
	--force \
	--schema ./schemas/gschemas.compiled \
	--schema ../src/schemas/org.gnome.shell.extensions.$(NAME).gschema.xml \
	--podir ../src/po \
	--gettext-domain "status-bar-system-monitor@elcodedocle.gitlab.gnome.org" \
	--extra-source status-bar-system-monitor.gresource \
	./dist
	@pushd dist && zip -r ../$(ZIP_FILE) ./modules && popd
	@mv $(ZIP_FILE) $(ZIP_NAME)

	@#Check if the zip size is too big to be uploaded
	@SIZE=$$(unzip -Zt $(ZIP_NAME) | awk '{print $$3}') ; \
	 if [[ $$SIZE -gt 5242880 ]]; then \
	    echo "ERROR! The extension is too big to be uploaded to" \
	         "the extensions website, keep it smaller than 5 MB!"; \
	    exit 1; \
	 fi

# Copies src/js files to dist folder; then removes unused legacy build files
js: js-copy js-legacy-remove

# Copies src/js files to dist folder;
# then replaces bootstrappers for legacy build;
# then transforms imports and exports across the codebase to legacy syntax
js-legacy: js-copy js-legacy-rename js-legacy-rewrite-imports-and-exports

# Copies src/js files to dist folder
js-copy:
	@echo "Copying js files..."
	@mkdir -p dist
	@cp -r src/js/* dist

# Removes unused legacy build files
js-legacy-remove:
	@rm dist/*_legacy.js

# Replaces bootstrappers for legacy build (extension.js with extension_legacy.js and prefs.js with prefs_legacy.js)
js-legacy-rename:
	@rename _legacy.js .js dist/*.js

# Transforms relative (to project /src/js folder) imports across the codebase to legacy syntax
js-legacy-rewrite-relative-imports:
	@find dist/. -type f -name "*.js" -print0 | xargs -0 \
	sed -ri \
	-e "s#import \{(.+?) as (.+?)\} from '[.]+/([^']*).js';#const \{\1: \2\} = imports.misc.extensionUtils.getCurrentExtension().imports.\3;#g" \
	-e "s#import (.+?) from '[.]+/([^']*/(.+?)).js';#const \1 = imports.misc.extensionUtils.getCurrentExtension().imports.\2.\3;#g" \
	-e "/const (.+?) = imports\.misc\.extensionUtils\.getCurrentExtension\(\)\.imports\.(.+?);/ { s#\.\./##g }" \
	-e "/const (.+?) = imports\.misc\.extensionUtils\.getCurrentExtension\(\)\.imports\.(.+?);/ { s#/#.#g }"

# Transforms imports and exports across the codebase to legacy syntax
js-legacy-rewrite-imports-and-exports: js-legacy-rewrite-relative-imports
	@find dist/. -type f -name "*.js" -print0 | xargs -0 \
	sed -ri \
	-e "s#import (.+?) from 'gi://\1';#const \1 = imports.gi.\1;#g" \
	-e "s#import \{gettext as _\} from 'resource:///org/gnome/shell/extensions/extension\.js';#const \{gettext: _\} = imports.misc.extensionUtils;#g" \
	-e "s#import \{gettext as _\} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs\.js';#const \{gettext: _\} = imports.misc.extensionUtils;#g" \
	-e "s#import [*] as (.+?) from 'resource:///org/gnome/shell/ui/(.+?)\.js';#const \1 = imports.ui.\2;#g" \
	-e "s#import \{(.+?) as (.+?)\} from 'resource:///org/gnome/shell/ui/(.+?)\.js';#const \{\1: \2\} = imports.ui.\3;#g" \
	-e "s#import (.+?) from 'resource:///org/gnome/shell/ui/(.+?)\.js';#const \1 = imports.ui.\2;#g" \
	-e "s#export (default )?class ([a-zA-Z0-9_]+?) (extends )?#var \2 = class \2 \3 #g" \
    -e "s#export const #var #g" \
    -e "s#export function #function #g"

# Copies src/css/stylesheet.css files to dist folder
dist/stylesheet.css:
	@echo "Copying stylesheet.css..."
	@mkdir -p dist
	@cp -r src/css/* dist

# Copies src/metadata.json files to dist folder
metadata:
	@echo "Copying metadata.json..."
	@mkdir -p dist
	@cp src/metadata.json dist

# Copies src/metadata_legacy.json files to dist/metadata.json
metadata-legacy:
	@echo "Copying metadata.json (legacy)..."
	@mkdir -p dist
	@cp src/metadata_legacy.json dist/metadata.json

# Compiles the gschemas.compiled file from the gschema.xml file.
dist/schemas/gschemas.compiled: src/schemas/org.gnome.shell.extensions.$(NAME).gschema.xml
	@echo "Compiling schemas..."
	@mkdir -p dist/schemas
	@glib-compile-schemas src/schemas --targetdir=dist/schemas

# Compiles the gresource file from the gresources.xml.
dist/$(NAME).gresource: dist/$(NAME).gresource.xml
	@echo "Compiling resources..."
	@glib-compile-resources --sourcedir="src/resources" dist/$(NAME).gresource.xml
	@rm dist/$(NAME).gresource.xml

# Generates the gresources.xml based on all files in the resources subdirectory.
dist/$(NAME).gresource.xml: $(RESOURCE_FILES)
	@echo "Creating resources xml..."
	@mkdir -p dist
	@FILES=$$(find "src/resources" -mindepth 2 -type f -printf "%P\n" | xargs -i echo "<file>{}</file>") ; \
	 echo "<?xml version='1.0' encoding='UTF-8'?><gresources><gresource prefix=\"/org/gnome/shell/extensions/elcodedocle/status-bar-system-monitor\"> $$FILES </gresource></gresources>" \
	     > dist/$(NAME).gresource.xml

# Copies the LICENSE file from the root folder to the dist folder.
dist/LICENSE:
	@echo "Copying LICENSE file..."
	@mkdir -p dist
	@cp LICENSE dist/LICENSE

# Copies the icons .license files from the src/resources/icons folder to the dist/resources/icons folder.
dist/resources/icons/*.license:
	@echo "Copying icons .license files..."
	@mkdir -p dist/resources/icons
	@cp src/resources/icons/*.license dist/resources/icons

# Compiles all *.po files to *.mo files.
dist/locale/%/LC_MESSAGES/$(NAME).mo: src/po/%.po
	@echo "Compiling $@"
	@mkdir -p dist/locale/$*/LC_MESSAGES
	@msgfmt -c -o $@ $<

# Deploys the package on a remote host (copy, install, and restart the gnome session to (re)load the extension).
# requires an ssh server, gnome-extensions, Xorg (not Wayland), and xdotool to be available on the target machine
deploy-common:
	@echo "Transferring zip to $(TARGET_HOST)..."
	@scp $(ZIP_NAME) "$(TARGET_HOST)":"./"
	@echo "Installing extension on $(TARGET_HOST)..."
	@ssh $(TARGET_HOST) "export DISPLAY=:0; gnome-extensions install \"./$(ZIP_FILE)\" --force && xdotool key \"Alt+F2+r\" && sleep 0.5 && xdotool key \"Return\""